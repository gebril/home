function check_all(){
    $('input[class=item_checked]:checkbox').each(function(){
      if($('input[class="checkall"]:checkbox:checked').length==0)
      {
        $(this).prop('checked',false);
      }else{
        $(this).prop('checked',true);
      }
    });
  }

  function delete_all(){
    $(document).on('click' , '#del_all' , function(){
        $('#form-data').submit();
    });

  	$(document).on('click','.delbtn',function(){
  		var item_checked = $('input[class=item_checked]:checkbox:checked').length;
  		if(item_checked > 0){
  			$('.record_count').text(item_checked);
  			$('.not_empty_record').removeClass('hidden');
  			$('.empty_record').addClass('hidden');
  		}else{
  			//$('.').text(item_checked);
  			$('.empty_record').removeClass('hidden');
  			$('.not_empty_record').addClass('hidden');
  		}
  		$('#multible').modal('show');
  	});
  }