$(document).ready(function() {

	$('#savechanges').click(function(){
				var name=$('#name').val();
				var email=$('#email').val();
				var password=$('#password').val();
				var id=$('#id').val();
				console.log(name);
				$.post(
					'/vision1/public/admin-panel/admin/update',
					{
						'id':id,
						'name':name,
						'email':email,
						'password':password,
						'_token':$('input[name=_token').val()
					},
					function(data){
						console.log(data);
						 $('#nameload').load(location.href + ' #nameload');
						 $('#nameload2').load(location.href + ' #nameload2');
						 $('#nameload3').load(location.href + ' #nameload3');
				});
			});

	// image preview
        $(".image_logo").change(function () {

            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.image-preview-logo').attr('src', e.target.result);
                }

                reader.readAsDataURL(this.files[0]);
            }

        });
        $(".image").change(function () {

            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.image-preview').attr('src', e.target.result);
                }

                reader.readAsDataURL(this.files[0]);
            }

        });
        $(".image-ar").change(function () {

            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.image-preview-ar').attr('src', e.target.result);
                }

                reader.readAsDataURL(this.files[0]);
            }

        });
        $(".image-en").change(function () {

            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.image-preview-en').attr('src', e.target.result);
                }

                reader.readAsDataURL(this.files[0]);
            }

        });
        $(".image_a").change(function () {

            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.image-preview-a').attr('src', e.target.result);
                }

                reader.readAsDataURL(this.files[0]);
            }

        });
        $(".image_b").change(function () {

            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.image-preview-b').attr('src', e.target.result);
                }

                reader.readAsDataURL(this.files[0]);
            }

        });
        $(".image_c").change(function () {

            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.image-preview-c').attr('src', e.target.result);
                }

                reader.readAsDataURL(this.files[0]);
            }

        });
        $(".image_d").change(function () {

            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.image-preview-d').attr('src', e.target.result);
                }

                reader.readAsDataURL(this.files[0]);
            }

        });

    // $('#add-form').submit(function(event) {
    //     event.preventDefault();
    //     $.ajax
    //     ({
    //         url: '/vision1/public/admin-panel/ours/items',
    //         type: 'POST',              
    //         data: {
    //             "_method": 'POST',
    //             "name": $('input[name=title]').val(),
    //             "logo": $('input[name=logo]').val(),
    //         },
    //         success: function(result)
    //         {
    //             console.log(result);
    //             location.reload();
    //         },
    //         error: function(data)
    //         {
    //             console.log(data);
    //         }
    //     });

    // });
    $('#addform').click(function(){
                var title=$('#title').val();
                var logo=$('#logo').val();
                console.log(title);
                $.post(
                    '/vision1/public/admin-panel/ours',
                    {
                        'title':title,
                        'logo':logo,
                        '_token':$('input[name=_token').val()
                    },
                    function(data){
                        console.log(data);
                         // $('#nameload').load(location.href + ' #nameload');
                         // $('#nameload2').load(location.href + ' #nameload2');
                         // $('#nameload3').load(location.href + ' #nameload3');
                });
            });

    //Edit
    $(document).on('click','#edit-our',function(event){  
                var id=$(this).find('#ourId').val();
                var title_ar=$(this).find('#title_ar').val();
                var title_en=$(this).find('#title_en').val();
                var img1=$(this).find('#img1').val();
                console.log(id);
                $('.modal-title').text('تعديل العنصر ');
                $('.titledit_ar').val(title_ar);
                $('.titledit_en').val(title_en);
                $('#edit-img-our').attr("src", img1);
                $('#id-our-edit').val(id);
        });
    $(document).on('click','#delete-our',function(event){  
                var id=$(this).find('#ourId').val();
                console.log(id);
                $('#our-my-id').val(id);
        });
    $(document).on('click','#plus-our',function(event){  
                var id=$(this).find('#ourId').val();
                console.log(id);
                $('#our-item-id').val(id);
        });
    // Hosts
    $(document).on('click','#delete-host',function(event){  
                var id=$(this).find('#hostId').val();
                console.log(id);
                $('#host-item-id').val(id);
        });
    // Editing
    $(document).on('click','#edit-host',function(event){  
                var id=$(this).find('#hostId').val();
                var title=$(this).find('#title').val();
                var title_en=$(this).find('#title_en').val();
                var cost=$(this).find('#cost').val();
                var image=$(this).find('#host_image').val();
                var hard_space=$(this).find('#hard_space').val();
                var traffic=$(this).find('#traffic').val();
                var ftp=$(this).find('#ftp').val();
                var email=$(this).find('#email').val();
                var email_space=$(this).find('#email_space').val();
                var database=$(this).find('#database').val();
                var subdomain=$(this).find('#subdomain').val();
                var support=$(this).find('#support').val();
                var host_id=$(this).find('#host_id').val();
                console.log(title);
                console.log(image);
                console.log(subdomain);
                $('.modal-title').text('تعديل الاسضافه ');
                $('#title_host').val(title);
                $('#title_en_host').val(title_en);
                $('#edit-img-host').attr("src", image);
                $('#id').val(id);
                $('#cost_host').val(cost);
                $('#hard_space_host').val(hard_space);
                $('#traffic_host').val(traffic);
                $('#ftp_host').val(ftp);
                $('#email_host').val(email);
                $('#email_space_host').val(email_space);
                $('#database_host').val(database);
                $('#subdomain_host').val(subdomain);
                if (support==1) {
                    $('#support_host').attr('checked','checked');
                }
                $('#host_id_edit').val(host_id);
        });
        // Hosts
        $(document).on('click','#delete-feature',function(event){  
                    var id=$(this).find('#feature-id').val();
                    $('#feature-id-form').val(id);
                    console.log(id);
            });
        // Categories
        $(document).on('click','#delete-cat',function(event){  
                    var id=$(this).find('#cat-id').val();
                    $('#cat-id-form').val(id);
                    console.log(id);
            });

        $(document).on('click','#edit-cat',function(event){  
                var id=$(this).find('#cat-id').val();
                var content_en=$(this).find('#cat-content-en').val();
                var content_ar=$(this).find('#cat-content-ar').val();
                console.log(id);
                console.log(content_en);
                console.log(content_ar);
                $('#content-en').val(content_en);
                $('#content-ar').val(content_ar);
                $('#edit-id-cat').val(id);
        });
        // Projects
        $(document).on('click','#delete-project',function(event){  
                    var id=$(this).find('#project-id').val();
                    $('#project-id-form').val(id);
                    console.log(id);
            });
        $(document).on('click','#edit-project',function(event){  
                var id=$(this).find('#id').val();
                var link=$(this).find('#link').val();
                var image=$(this).find('#image').val();
                console.log(id);
                $('#project-edit-link').val(link);
                $('#project-edit-image').attr("src", image);
                $('#edit-id-project').val(id);
        });
        //Blog Categories
        $(document).on('click','#delete-blogcat',function(event){  
                    var id=$(this).find('#blogcat-id').val();
                    $('#blogcat-id-form').val(id);
                    console.log(id);
            });

        $(document).on('click','#edit-blogcat',function(event){  
                var id=$(this).find('#blogcat-id').val();
                var content_en=$(this).find('#blogcat-content-en').val();
                var content_ar=$(this).find('#blogcat-content-ar').val();
                console.log(id);
                console.log(content_en);
                console.log(content_ar);
                $('#content-en').val(content_en);
                $('#content-ar').val(content_ar);
                $('#edit-id-blogcat').val(id);
        });

        $(document).on('click','#delete-blog',function(event){  
                    var id=$(this).find('#blog-id').val();
                    $('#blog-id-form').val(id);
                    console.log(id);
            });
        $(document).on('click','#delete-email',function(event){  
                    var id=$(this).find('#emailId').val();
                    $('#email-id').val(id);
                    console.log(id);
            });
});