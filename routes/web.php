<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(
[
	'prefix' => LaravelLocalization::setLocale(),
	'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
],
function(){
		Route::get('/','FrontController@index');
		Route::get('index','FrontController@index');
		Route::get('videos','FrontController@videos');	
		Route::get('news','FrontController@news');	
		Route::get('clients','FrontController@clients');	
		Route::get('jobs','FrontController@job');	
		Route::post('add-job','FrontController@add_job');	
		Route::get('contact-us','FrontController@contact_us');
		Route::post('send-message','FrontController@send_message');
		Route::get('{title}','FrontController@page');	
		Route::get('slider/{title}','FrontController@slider');	
		Route::get('news/{title}','FrontController@one_news');	
		Route::get('blogs','FrontController@blogs');	
		Route::get('blogs/{name}/{id}','FrontController@one_blog');	
		Route::get('ecommerce-sites','FrontController@ecommerce');	
		Route::get('hosts','FrontController@hosts');	
		Route::get('webdesign','FrontController@webdesign');	
		Route::get('governmentsites','FrontController@governmentsites');	
		Route::get('webperson','FrontController@webperson');	
		Route::get('webcompany','FrontController@webcompany');	
		Route::get('motiongraphics','FrontController@motiongraphics');	
		Route::get('graphicdesign','FrontController@graphicdesign');	
		Route::get('googlemarketing','FrontController@googlemarketing');	
		Route::get('googlesearch','FrontController@googlesearch');	
		Route::get('instagrammarketing','FrontController@instagrammarketing');	
		Route::get('marketing','FrontController@marketing');	
		Route::get('snapchat','FrontController@snapchat');	
		Route::get('socialmedia','FrontController@socialmedia');	
		Route::get('twittermarketing','FrontController@twittermarketing');	
		Route::get('youtubemarketing','FrontController@youtubemarketing');	
		Route::get('aboutvision','FrontController@aboutvision');	
		Route::get('private-policy','FrontController@policy');	
		Route::post('send-mail','FrontController@send_mail');	
		Route::get('send-mail',function(){
			Mail::to('anhar500.am@gmail.com')->send(new App\Mail\Contact());
			return 'hahahahhahahaaha';
		});	
		Route::post('send-newsletter','FrontController@newsletter');

/*

	SEO Projects

*/
});


// <script>
//   window.fbAsyncInit = function() {
//     FB.init({
//       appId      : '{your-app-id}',
//       cookie     : true,
//       xfbml      : true,
//       version    : '{api-version}'
//     });
      
//     FB.AppEvents.logPageView();   
      
//   };

//   (function(d, s, id){
//      var js, fjs = d.getElementsByTagName(s)[0];
//      if (d.getElementById(id)) {return;}
//      js = d.createElement(s); js.id = id;
//      js.src = "https://connect.facebook.net/en_US/sdk.js";
//      fjs.parentNode.insertBefore(js, fjs);
//    }(document, 'script', 'facebook-jssdk'));
// </script>
		
