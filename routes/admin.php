<?php



Route::group(
[
	'prefix' => LaravelLocalization::setLocale(),
	'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
],
function(){
	
	Route::group(['prefix'=>'admin-panel'],function(){

		Route::group(['middleware'=>'notadmin'],function(){
			Route::get('/login','LoginController@login_page');
			Route::post('/dologin','LoginController@login');
		});
		
		Route::group(['middleware'=>'admin'],function(){
			Route::any('logout','LoginController@logout');
			Route::get('admin/edit','AdminController@edit');
			Route::put('admin/update','AdminController@update');
			Route::get('admin','AdminController@index');
			//Start Slider
			Route::get('index/sliders','SliderController@index');
			Route::get('index/create-slider','SliderController@create');
			Route::get('index/edit-slider/{id}','SliderController@edit');
			Route::post('index/store-slider','SliderController@store');
			Route::put('index/update-slider/{id}','SliderController@update');
			Route::post('index/delete-slider','SliderController@delete');
			Route::get('index/slider-api','SliderController@slider_api');
			Route::get('index/base-data','BaseController@index');
			Route::post('base-data/update','BaseController@update');
			Route::get('cvs-api','BaseController@cvs_api');
			Route::get('cvs','BaseController@cvs');
			Route::get('cv-preview/{id}','BaseController@cv_preview');
			Route::post('delete-cv','BaseController@cv_delete');
			// End Slider
			//Start Blog
			Route::get('blog-categories','BlogController@categories');
			Route::post('Add-blogcategory','BlogController@add_category');
			Route::post('delete-blogcategory','BlogController@delete_category');
			Route::post('update-blogcategory','BlogController@update_category');
			Route::get('blogs/{type}','BlogController@blogs');
			Route::post('blog/add','BlogController@add_blog');
			Route::post('delete-blog','BlogController@delete_blog');
			Route::get('blog-seo','BlogController@blogseo');
			Route::post('update-blogseo', 'BlogController@blogseo_update');
			Route::get('edit-blog/{id}', 'BlogController@blog_edit');
			Route::put('blog-update/{id}', 'BlogController@blog_update');
			Route::get('blog-projects/{type}/{id}', 'BlogController@blog_add_projects');
			Route::post('blog-projects/{id}', 'BlogController@blog_add_projects_post');
			//End Blog
			/* Start Clients*/
			Route::get('clients','ClientController@index');
			Route::get('edit-client/{id}','ClientController@edit');
			Route::post('client/add','ClientController@store');
			Route::post('delete-client','ClientController@delete');
			Route::put('client-update/{id}','ClientController@update');
			Route::get('clients-seo','ClientController@seo');
			Route::put('clients-seo/update/{id}','SeoController@update');

			/*End Clients*/
			/*seo*/
 			Route::get('services-seo','SeoController@service_seo');
 			Route::get('jobs-seo','SeoController@job_seo');
 			Route::get('news-seo','SeoController@news_seo');
 			Route::get('tv-seo','SeoController@tv_seo');
 			Route::get('contactus-seo','SeoController@contactus_seo');
			/* Videos in Media Center*/
			Route::get('tv-videos','TvController@index');
			Route::post('tv-store','TvController@store');
			Route::post('delete-tv','TvController@delete');
			Route::get('edit-tv/{id}','TvController@edit');
			Route::put('update-tv/{id}','TvController@update');
			/*End Videos*/
			Route::get('index/about-us','AboutController@index');
			Route::post('about-us/update','AboutController@update');

			Route::get('newsletter','AdminController@newsletter');
			Route::get('api/newsletter', 'AdminController@api_newsletter')->name('api.newsletter');
			Route::post('newsletter-delete', 'AdminController@delete_email');
			// Contract
			
		});
	});

	
});



// <IfModule mod_rewrite.c>
//    RewriteEngine On 
//    RewriteRule ^(.*)$ public/$1 [L]
// </IfModule>

// anhar500.am@gmail.com
// amr123456789

?>