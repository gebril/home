@extends('front.master')
@section('content')
<!--===== PAGE TITLE =====-->
<div class="page-title page12 page-main-section">
  <div class="padding-bottom-top-120 text-uppercase text-center">
    <div class="main-title">
      <h1>عن اسناد</h1>
      <a href="index.html">الرئيسية</a>
      <span><i class="fa fa-angle-double-left" aria-hidden="true"></i></span>
      <a href="#">عن اسناد</a> 
    </div>
  </div>
</div>
<!--===== PAGE TITLE =====-->

<!--===== ABOUT US =====-->
<section id="about_us" class="about-us padding">
  <div class="container">
    <div class="row">
      <div class="history-section">
		 <div class="col-md-5 col-sm-5 col-xs-12">
          <div id="about_single">
            <div class="item">
              <div class="content-right-md">
                <figure class="effect-layla">
                  <img src="{{url('upload/'.$blog->image)}}" alt="عن اسناد"/>
                  <figcaption> </figcaption>
                </figure>
              </div>
            </div>
            
          </div>
        </div>
     
        <div class="col-md-7 col-sm-7 col-xs-12">
          <h4 class="text-title">{{$blog->{'title:'.app()->getLocale()} }}</h4>
          
          <p class="top20 bottom40">{!!$blog->{'desc:'.app()->getLocale()} !!}</p>
        </div>
       
	 </div>
			
         
	 
	</div>
  </div>
</section>
<!--===== #/ABOUT US =====-->
@endsection