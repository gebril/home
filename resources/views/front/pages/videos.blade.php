@extends('front.master')
@section('content')
<!--===== PAGE TITLE =====-->
<div class="page-title videos page-main-section">
  <div class="padding-bottom-top-120 text-uppercase text-center">
    <div class="main-title">
      <h1>@lang('site.tv_videos')</h1>
      <a href="{{murl('/')}}">@lang('site.home')</a>
      <span><i class="fa fa-angle-double-left" aria-hidden="true"></i></span>
      <a href="{{murl('videos')}}">@lang('site.tv_videos')</a> 
    </div>
  </div>
</div>
<!--===== PAGE TITLE =====-->

<!--===== TEAM  =====-->
<section class="padding_top padding_bottom" id="teams">
  <div class="container">
    <div class="row">
    @foreach($videos as $video)
	 	<div class="col-md-6">
			<div class="property_item heading_space">
	            <div class="image">
	                <iframe width="560" height="315" src="https://www.youtube.com/embed/{{$video->video}}" frameborder="0" allowfullscreen></iframe>
	            </div>
	            <div class="property_meta">
	              	<a href="#">{{$video->{'title:'.app()->getLocale()} }}</a>
	            </div>
			</div>
		</div>
	@endforeach
	</div>
    
  </div>
</section>
<!--===== #/TEAM  =====-->

@endsection