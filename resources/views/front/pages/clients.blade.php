@extends('front.master')
@section('content')
<!--===== PAGE TITLE =====-->
<div class="page-title client page-main-section">
  <div class="padding-bottom-top-120 text-uppercase text-center">
    <div class="main-title">
      <h1>@lang('site.clients')</h1>
      <a href="index.html">@lang('site.home')</a>
      <span><i class="fa fa-angle-double-left" aria-hidden="true"></i></span>
      <a href="#">@lang('site.clients')</a> 
    </div>
  </div>
</div>
<!--===== PAGE TITLE =====-->

<!--===== ABOUT US =====-->
<section id="client-page" class="about-us padding">
  <div class="container">
    <div class="row">
        @foreach($clients as $client)
        <div class="col-md-3">
			<div class="item"><img src="{{url('upload/'.$client->image)}}" alt="{{$client->{'alt:'.app()->getLocale()} }}" style="height: 70px;"></div>
		</div>
		@endforeach
	</div>
  </div>
</section>
<!--===== #/ABOUT US =====-->

@endsection