@extends('front.master')
@section('content')
<section class="rev_slider_wrapper">
  <div id="rev_slider_3" class="rev_slider"  data-version="5.0">
    <ul>
      @foreach($slider->gallories as $gal)
      <li data-transition="fade">
        <img src="{{url('upload/'.$gal->image)}}"  alt="" data-bgposition="center center" data-bgfit="cover" class="rev-slidebg">
      </li>
      @endforeach
    </ul>
  </div>
</section>


<!--  Home Icons Start  -->
<div id="home_icon-slider">
  <div class="container">

   <p class="p-home">
      {!! $slider->{'desc:'.app()->getLocale()} !!}
   </p>
<!--  Home Icons End  -->

</div>
</div>

<!-- OUR PARTNERS -->
<section id="our-partner">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div id="partner_slider" class="owl-carousel">
          @foreach($clients as $client)
            <div class="item"><img src="{{url('upload/'.$client->image)}}" alt="{{$client->alt}}"></div>  
          @endforeach               
        </div>
      </div>
    </div>
  </div>
</section>
<!-- #/OUR PARTNERS -->
@endsection