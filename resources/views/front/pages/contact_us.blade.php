@extends('front.master')
@section('content')
<!--===== PAGE TITLE =====-->
<div class="page-title page-main-section">
  <div class="padding-bottom-top-120 text-uppercase text-center">
    <div class="main-title">
      <h1>@lang('site.contact_us')</h1>
      <a href="#">@lang('site.home')</a>
      <span><i class="fa fa-angle-double-left" aria-hidden="true"></i></span>
      <a>@lang('site.contact_us')</a> 
    </div>
  </div>
</div>
<!--===== PAGE TITLE =====-->

<!--===== CONTACT US =====-->
<section id="contact-us">
	<div class="container">
      <div class="row padding">
      	
        <div class="col-md-8">
        	<div class="bottom40">
                <h2 class="text-uppercase">@lang('site.send_us')<span class="color_red">@lang('site.msg')  </span></h2>
                <div class="line_1"></div>
                <div class="line_2"></div>
                <div class="line_3"></div>
              </div>
        	<div class="agent-p-form p-t-30">
            
            <div class="row">
                @if($errors->any())
                    <div class="alert alert-danger" style="padding: 3px;">
                        <ul style="    margin-bottom: 0;">
                            @foreach ($errors->all() as $error)
                                <li class="text-center">{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            	<form class="callus padding-bottom"  id="contact-form"  action="{{murl('send-message')}}" method="post">
                      @csrf
                     <div class="form-group">
                         <div id="result">
                         </div>
                     </div>
            
            	<div class="col-md-12">
                    <div class="single-query">
                        <input type="text" required="required" class ="keyword-input" placeholder="@lang('site.name')" name="name" id="name">
                    </div>
                </div>
               <div class="col-md-12">    
                    <div class="single-query">
                        <input type="text" required="required" class ="keyword-input" placeholder="@lang('site.phone_number')" name="phone" id="phone">
                    </div>
               </div>
               <div class="col-md-12">     
                    <div class="single-query">
                        <input type="email" required="required" class ="keyword-input" placeholder="@lang('site.email')" name="email" id="email">
                    </div>
               </div>
               <div class="col-md-12">
                    <div class="single-query">
                        <textarea name="message" required="required" placeholder="@lang('site.msg')" id="message"></textarea>
                    </div>
               </div>
                 <div class="col-md-12">   
                      <button type="submit" class="btn_fill" id="btn_submit" name="btn_submit">@lang('site.send') </button>
                 </div>     
                    </form>
        
            </div>
          </div>
        </div>
        
        <div class="col-md-4">
        	<div class="bottom40">
                <h2 class="text-uppercase">@lang('site.contact_to_us')<span class="color_red"> @lang('site.esnad') </span></h2>
                <div class="line_1"></div>
                <div class="line_2"></div>
                <div class="line_3"></div>
              </div>
              
        	<div class="agent-p-contact p-t-30">
            <div class="agetn-contact-2">
                          <p><i class="icon-telephone114"></i>{{$base->number1}}</p>
              <a href="#.">
                <p><i class=" icon-icons142"></i>{{$base->email}}</p>
              </a>
              <a href="index-2.html">
                <p><i class="icon-browser2"></i>wwww.esnadrealestate.com</p>
              </a>
              <p><i class="icon-icons74"></i>{{$base->address}}</p>
            </div>
            <ul class="socials">
              <li><a href="{{$base->facebook}}"><i class="fa fa-facebook"></i></a></li>
              <li><a href="{{$base->twitter}}"><i class="fa fa-twitter"></i></a></li>
              <li><a href="{{$base->youtube}}"><i class="fa fa-youtube"></i></a></li>
              <li><a href="{{$base->instgram}}"><i class="fa fa-instagram"></i></a></li>
            
            
            </ul>
          </div>
        </div>
       
      </div>
    </div>
    
    <div class="contact">
      <div id="map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3708.899452044201!2d39.136118885057016!3d21.628841385676363!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjHCsDM3JzQzLjgiTiAzOcKwMDgnMDIuMiJF!5e0!3m2!1sar!2ssa!4v1510053050575" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>
    </div>
</section>
<!--===== #/CONTACT US =====-->
@endsection