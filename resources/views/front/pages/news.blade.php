@extends('front.master')
@section('content')
@if(app()->getLocale()=='ar')
<!--===== PAGE TITLE =====-->
<div class="page-title news page-main-section">
  <div class="padding-bottom-top-120 text-uppercase text-center">
    <div class="main-title">
      <h1>@lang('site.news_word')</h1>
      <a href="{{murl('/')}}">@lang('site.home')</a>
      <span><i class="fa fa-angle-double-left" aria-hidden="true"></i></span>
      <a href="#">@lang('site.news_word')</a> 
    </div>
  </div>
</div>
<!--===== PAGE TITLE =====-->

<!--===== TEAM  =====-->
<section class="padding_top padding_bottom" id="teams">
  <div id="cont-job" class="container">
    <div class="row">
    	@foreach($news as $new)
      	<div class="col-md-6">
			<div class="block-m">
				<div class="block-text">
					<h3><a href="{{murl('news/'.$new->{'title:'.app()->getLocale()} )}}">{{$new->{'title:'.app()->getLocale()} }}</a></h3>
					<p><?php $text=strip_tags($new->{'desc:'.app()->getLocale()}); ?>
           {{preg_replace("/\s|&nbsp;/",' ',$text)}}
          </p>
				</div>
			</div>
		</div>	
		@endforeach
	</div>
    
  </div>
</section>
<!--===== #/TEAM  =====-->
@else
<!--===== PAGE TITLE =====-->
<div class="page-title page-main-section">
  <div class="container padding-bottom-top-120 text-uppercase text-center">
    <div class="main-title">
      <h1>@lang('site.news_word')</h1>
      <a href="{{murl('/')}}">@lang('site.home')</a>
      <span><i class="fa fa-angle-double-left" aria-hidden="true"></i></span>
      <a href="#">@lang('site.news_word')</a> 
    </div>
  </div>
</div>
<!--===== PAGE TITLE =====-->

<!--===== TEAM  =====-->
<section class="padding_top padding_bottom" id="teams">
  <div class="container">
    <div class="row">
      @foreach($news as $new)
	  	<div class="col-md-6">
			<div class="property_item heading_space">
                <div class="property_meta">
                  <a href="{{murl('news/'.$new->{'title:'.app()->getLocale()} )}}">{{$new->{'title:'.app()->getLocale()} }}</a>
                </div>
                <div class="proerty_content">
                  <div class="proerty_text">
                    <p>{{strip_tags(substr_replace($new->{'desc:'.app()->getLocale()},"...",500))}}</p>
                  </div>
                </div>
            </div>
	  	</div>
	  	@endforeach
    </div>
    
  </div>
</section>
<!--===== #/TEAM  =====-->
@endif
@endsection