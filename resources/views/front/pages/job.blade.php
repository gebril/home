@extends('front.master')
@section('content')
<!--===== PAGE TITLE =====-->
<div class="page-title jobs page-main-section">
  <div class="padding-bottom-top-120 text-uppercase text-center">
    <div class="main-title">
      <h1>@lang('site.jobs2')</h1>
      <a href="#">@lang('site.home')</a>
      <span><i class="fa fa-angle-double-left" aria-hidden="true"></i></span>
      <a >@lang('site.jobs2')</a> 
    </div>
  </div>
</div>
@if($errors->any())
    <div class="alert alert-danger" style="padding: 3px;">
        <ul style="    margin-bottom: 0;">
            @foreach ($errors->all() as $error)
                <li class="text-center">{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form id="itemForm" class="span12 form-horizontal"  action="{{murl('add-job')}}"  method="post" enctype="multipart/form-data" >
	@csrf
	<section class="cont-pp">
	    
	    <div class="container">
	       <h3>@lang('site.upload_cv') </h3>
	        <div class="single-query">
	                        <input type="text" class="keyword-input" placeholder="{{trans('site.your_name')}}" name="name" id="Title" value="{{old('name')}}">
	       </div>
	       
	       <div class="single-query">
	                        <input type="text" class="keyword-input" placeholder="{{trans('site.job_title')}}" name="job_title" id="Header" value="{{old('name')}}">
	       </div>
	       
	       <div class="single-query">
	                        <input class="keyword-input"  id="fileInput" name="cv" type="file">
	       </div>
	       <div class="single-query">
	                       <button tybe="submit">@lang('site.attach_now')</button>
	       </div>
	        
	    
	    </div>
	</section>
</form>
@endsection