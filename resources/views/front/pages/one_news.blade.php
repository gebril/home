@extends('front.master')
@section('content')
@if(app()->getLocale()=='ar')
<!--===== PAGE TITLE =====-->
<div class="page-title page-main-section">
  <div class="padding-bottom-top-120 text-uppercase text-center">
    <div class="main-title">
      <h1></h1>
      <a href="index.html">@lang('site.home')</a>
      <span><i class="fa fa-angle-double-left" aria-hidden="true"></i></span>
      <a></a> 
    </div>
  </div>
</div>
<!--===== PAGE TITLE =====-->

<!--===== ABOUT US =====-->
<section id="about_us" class="about-us padding">
  <div class="container">
    <div class="row">
      <div class="history-section">
         
        <div class="col-md-12 col-sm-12 col-xs-12">
          <h4 class="text-title">{{$blog->{'title:'.app()->getLocale()} }}</h4>
          
          <p class="top20 bottom40">{!!$blog->{'desc:'.app()->getLocale()} !!}</p>
        </div>
    <!-- <div>
      <h3>التقديم للوظيفة</h3>
      <div class="agent-p-form p-t-30">
            
            <div class="row">
              <form class="callus padding-bottom" id="contact-form" onsubmit="return false">
            
                     <div class="form-group">
                         <div id="result">
                         </div>
                     </div>
            
              <div class="col-md-12">
                    <div class="single-query">
                        <input type="text" class="keyword-input" placeholder="الاسم" name="name" id="name">
                    </div>
                </div>
               <div class="col-md-12">    
                    <div class="single-query">
                        <input type="text" class="keyword-input" placeholder="الهاتف" name="phone" id="phone">
                    </div>
               </div>
               <div class="col-md-12">     
                    <div class="single-query">
                        <input type="email" class="keyword-input" placeholder="بريدك الإلكتروني" name="email" id="email">
                    </div>
               </div>
         <div class="col-md-12">     
                    <div class="single-query">
                        <label>السيرة الذاتية : </label> <input type="file" class="keyword-input" placeholder="بريدك الإلكتروني" name="email" id="email">
                    </div>
               </div>
               
                 <div class="col-md-12">   
                      <button type="submit" class="btn_fill" id="btn_submit" name="btn_submit">ارسل </button>
                 </div>     
                    </form>
        
            </div>
      
          </div> -->
    </div>
       
   </div>
      
         
   
  </div>
  </div>
</section>
<!--===== #/ABOUT US =====-->
@else
<!--===== PAGE TITLE =====-->
<div class="page-title page-main-section">
  <div class="container padding-bottom-top-120 text-uppercase text-center">
    <div class="main-title">
      <h1>{{$blog->{'title:'.app()->getLocale()} }}</h1>
      <a href="index-2.html">@lang('site.home')</a>
      <span><i class="fa fa-angle-double-left" aria-hidden="true"></i></span>
      <a href="#">{{$blog->{'title:'.app()->getLocale()} }}</a> 
    </div>
  </div>
</div>
<!--===== PAGE TITLE =====-->

<!--===== ABOUT US =====-->
<section id="about_us" class="about-us padding">
  <div class="container">
    <div class="row">
      <div class="history-section">
     <div class="col-md-5 col-sm-5 col-xs-12">
          <div id="about_single" class="owl-carousel">
            <div class="item">
              <div class="content-right-md">
                <figure class="effect-layla">
                  <img src="{{url('upload/'.$blog->image)}}" alt="Babahr, Author , Speaker, Trainer and Consultant"/>
                  <figcaption> </figcaption>
                </figure>
              </div>
            </div>
            
          </div>
        </div>
     
        <div class="col-md-7 col-sm-7 col-xs-12">
          <h4 class="text-title">{{$blog->{'title:'.app()->getLocale()} }}</h4>
          
          <p class="top20 bottom40">{!!$blog->{'desc:'.app()->getLocale()} !!}</p>
        </div>
       
   </div>
      
         
   
  </div>
  </div>
</section>
<!--===== #/ABOUT US =====-->
@endif
@endsection