<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    

@if(isset($seo_title))
    <title>{{ $seo_title }}</title>
    <meta name="title" content="{{$seo_title}}">
     @endif
     @if(isset($seo_desc))
     <meta name="description" content="{{$seo_desc}}">
     @endif
     @if(isset($seo_keywords))
     <meta name="keywords" content="{{$seo_keywords}}">
     @endif
    <link rel="stylesheet" href="{{url('public/front/')}}/css/bootstrap.css">
    @if(app()->getLocale()=='ar')
    <link href="https://fonts.googleapis.com/css2?family=Cairo&display=swap" rel="stylesheet">
    @else
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500&display=swap" rel="stylesheet">
    @endif
    <link rel="stylesheet" href="{{url('public/front/')}}/css/style.css">
@if(app()->getLocale()=='ar')
        <link rel="stylesheet" href="{{url('public/front/')}}/css/style-ar.css">

        <link href="{{url('public/front/')}}/css/bootstrap-rtl.css" rel="stylesheet">
    @endif
    <script src="{{url('public/front/')}}/js/html5shiv.min.js"></script>
    <script src="{{url('public/front/')}}/js/respond.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
@if(app()->getLocale()=='ar')

<style type="text/css" media="screen">
  * {
    font-family: 'Cairo', sans-serif;
  }
</style>
@else
<style type="text/css" media="screen">
  * {
    font-family: 'Montserrat', sans-serif;
  }
</style>
@endif
</head>
<body style="height:1449px;">
    <div class="navv">
    <div class="container ">
        <nav class="navbar navbar-expand-lg navbar-dark ">
            <div class="logo-div">
                <a class="navbar-brand" href="{{url('/')}}">
                  @if(app()->getLocale()=='ar')
                    <img src="{{url('public/front/images/logo-ar.png' )}}" alt="">
                  @else
                    <img src="{{url('public/front/images/logo.png' )}}" alt="">
                  @endif
                </a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                  <li class="nav-item ">
                    <a class="nav-link" href="{{url('/')}}" ><span class="list-item-home current">@lang('site.home')</span> </a>
                  </li>
                  <li class="nav-item ">
                    <a class="nav-link" > <span class="list-item-ser "> @lang('site.our_service')</span></a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" ><span class="list-item-cont ">@lang('site.contact_us')</span></a>
                  </li>
                  <li class="nav-item ">

                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                        @if(app()->getLocale()=='en'&&$localeCode=='en')
                          <?php continue; ?>
                          <a class="nav-link language " href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                            ar
                          </a>
                          @else
                          <a class="nav-link language " href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                            @if(app()->getLocale()=='ar')
                              en
                            @else
                              ar
                            @endif
                          </a>
                          <?php break; ?>
                          @endif

                        @endforeach
                  </li>
                </ul>
              </div>

          </nav>
        </div>
        </div>
        <!-- start works -->
        <div class="container">
        <div class="works ">
            <div class="row">
            <div class=" col-lg-3">
                <div class="header">
                <h1>
@lang('site.latest_work')
                </h1>
            </div>
            </div>
            <div class="col-lg-9">
            <div class="slider">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">

                    <div class="carousel-inner">
                      <?php $x=1; ?>
                      @foreach($projects as $project)

                        <div class="carousel-item {{$x==1?'active':''}}">
                          <?php $x++; ?>
                          <a href="{{murl(str_replace(' ','_',$project->{'title:'.app()->getLocale()})) }}">
                            <img class="d-block w-100" src="{{url('public/upload/'.$project->image)}}" alt="{{$project->{'title:'.app()->getLocale()} }}" style="height: 433px">
                          </a>
                        </div>

                      @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true" style="background-color: chocolate; "></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true" style="background-color: chocolate; "></span>
                      <span class="sr-only">Next</span>
                    </a>
                    <ol class="carousel-indicators">
                      <?php $x=1; ?>
                      @foreach($projects as $project)
                        <li data-target="#carouselExampleIndicators" data-slide-to="{{ $x }}" class="{{ $x==1?'active':'' }}"></li>
                        <?php $x++; ?>
                      @endforeach
                      </ol>
                  </div>
            </div>
            </div>
        </div>
    </div>
    </div>
        <!-- end works -->
        <!-- start services -->
    <div class="container" id="servicesss">
        <div class="services">
            <div class="header">
                <h1>
                    @lang('site.our_service')
                </h1>
            </div>
            @if(app()->getLocale()=='ar')
            <div class="row">
              @foreach($services as $service)
                <a class="col-lg-4" href="{{murl(str_replace(' ','_',$service->{'title:'.app()->getLocale()})) }}">
                    <div class="ser-div">
                        <div class="ser-back" style="height: 302px;padding-top: 230px;
    margin-top: 0px;">{{$service->{'title:'.app()->getLocale()} }}</div>

                        <img src="{{url('public/upload/'.$service->image)}}" alt="" style="height: 300px;">

                    </div>
                </a>
              @endforeach
            </div>
            @else
            <div class="row">
              @foreach($services as $service)
                        <a class="col-lg-4" href="{{murl(str_replace(' ','_',$service->{'title:'.app()->getLocale()})) }}">

                    <div class="ser-div">
                        <div class="ser-back" style="height: 302px;padding-top: 236px;">{{$service->{'title:'.app()->getLocale()} }}</div>
                            <img src="{{url('public/upload/'.$service->image)}}" alt="" style="height: 300px;">
                    </div>

                        </a>
              @endforeach
            </div>
            @endif
        </div>
    </div>

        <!-- end services -->
        <!-- start contact -->
        <div class="container" id="contact_usss">

            <div class="row">
                <div class="col-lg-4">
                <div class="logo">
                    @if(app()->getLocale()=='ar')
                      <img src="{{url('public/front/images/logo-ar.png' )}}" alt="">
                    @else
                      <img src="{{url('public/front/images/logo.png' )}}" alt="">
                    @endif
                </div>
                </div>
                <div class="col-lg-8">
                <div class="contact-form">
                    @if (count($errors) > 0)
                      @foreach ($errors->all() as $item)
                        <p class="alert alert-danger">{{$item}}</p>
                      @endforeach
                    @endif
                    <form class="contact-form" action="{{murl('send-message')}}" method="post">
                    @csrf
                    <h2> @lang('site.contact_us')</h2>
                    <div class="form">
                        <div>
                        <label for="email"> @lang('site.email')</label>
                        <input type="text" id="email" placeholder="{{__('site.email_enter')}}" name="email" required="required">
                        </div>
                        <div>
                        <label for="MobileNO">@lang('site.phone')</label>
                        <input type="text" id="MobileNO" placeholder="{{__('site.optional')}}" name="phone" required="required">
                        </div>
                        <div>
                        <label for="message">@lang('site.msg')</label>
                        <textarea id="subject" name="message" placeholder="{{__('site.write_msg')}}" style="height:200px"></textarea>
                        </div>
                        <div class="button">
                            <input type="submit" value="{{__('site.contact_us')}}
">
                        </div>
                    </div>
                    </form>
                </div>
            </div>


            <div class="contact-form-mobile">
                <h2>@lang('site.contact_us')</h2>
                <div class="form-mobile">
                    @if (count($errors) > 0)
                      @foreach ($errors->all() as $item)
                        <p class="alert alert-danger">{{$item}}</p>
                      @endforeach
                    @endif
                    <form  action="{{murl('send-message')}}" method="post">
                    @csrf
                    <div>
                    <label for="email">@lang('site.email')</label>
                    </div>
                    <div>
                    <input type="text" id="email" placeholder="{{__('site.email_enter')}}" name="email" required="required">
                    </div>
                    <div>
                    <label for="MobileNO">@lang('site.phone')</label>
                    </div>
                    <div>
                    <input type="text" id="MobileNO" placeholder="{{__('site.optional')}}" name="phone" required="required">
                    </div>
                    <div>
                    <label for="message">@lang('site.msg')</label>
                    </div>
                    <div>
                    <textarea id="subject" name="message" placeholder="{{__('site.write_msg')}}" style="height:200px"></textarea>
                    </div>
                    <div class="button">
                        <input type="submit" value="{{__('site.contact_us')}}">
                    </div>
                    </form>
                </div>
            </div>
        </div>

        </div>
        <!-- end contact -->
<!-- start footer -->
        <div class="footer">
            <div class="social-media">
                <a href="{{$base->facebook}}">
                    <img src="{{url('public/front/')}}/images/facebook-icon.png" alt="">
                </a>
                <a href="{{$base->twitter}}">
                    <img src="{{url('public/front/')}}/images/twitter-icon.png" alt="">
                </a>
                <a href="call:{{$base->number1}}">
                    <img src="{{url('public/front/')}}/images/tel-icon.png" alt="">
                </a>
            </div>
        </div>
<!-- end footer -->

    <script  src="{{url('public/front/')}}/js/jquery.js"></script>
    <script src="{{url('public/front/')}}/js/bootstrap.min.js"></script>
    <script src="{{url('public/front/')}}/js/main.js"></script>
    @if (session('sending_success'))

    <script>
        Swal.fire({
                  title: '{{trans("site.success")}}',
                  text: "{{session('message_success')}}",
                  type: 'success',
                  showCancelButton: false,
                  confirmButtonColor: '#5c5858',
                  confirmButtonText: '{{trans("site.ok")}}'
                })
    </script>

@endif
</body>
</html>
