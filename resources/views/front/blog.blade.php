<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $blog->{'title:'.app()->getLocale()} }}</title>
    @if(isset($seo_title))
    <meta name="title" content="{{$seo_title}}">
     @endif
     @if(isset($seo_desc))
     <meta name="description" content="{{$seo_desc}}">
     @endif
     @if(isset($seo_keywords))
     <meta name="keywords" content="{{$seo_keywords}}">
     @endif
      
    <link rel="stylesheet" href="{{url('public/front/')}}/css/bootstrap.css">
    @if(app()->getLocale()=='ar')
    <link href="https://fonts.googleapis.com/css2?family=Cairo&display=swap" rel="stylesheet">
    @else
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500&display=swap" rel="stylesheet">
    @endif
    <link rel="stylesheet" href="{{url('public/front/')}}/css/style.css">
@if(app()->getLocale()=='ar')
        <link rel="stylesheet" href="{{url('public/front/')}}/css/style-ar.css">

        <link href="{{url('public/front/')}}/css/bootstrap-rtl.css" rel="stylesheet">
    @endif
    <script src="{{url('public/front/')}}/js/html5shiv.min.js"></script>
    <script src="{{url('public/front/')}}/js/respond.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
@if(app()->getLocale()=='ar')

<style type="text/css" media="screen">
  * {
    font-family: 'Cairo', sans-serif;
  }
</style>
@else
<style type="text/css" media="screen">
  * {
    font-family: 'Montserrat', sans-serif;
  }
</style>
@endif
</head>
<body style="height: 1449px;"> 
    <div class="navv">
    <div class="container ">
        <nav class="navbar navbar-expand-lg navbar-dark ">
            <div class="logo-div">
                <a class="navbar-brand" href="{{url('/')}}">
                  @if(app()->getLocale()=='ar')
                    <img src="{{url('public/front/images/logo-ar.png' )}}" alt="">
                  @else
                    <img src="{{url('public/front/images/logo.png' )}}" alt="">
                  @endif
                </a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                  <li class="nav-item ">
                    <a class="nav-link" href="{{url('/')}}" ><span class="list-item-home current">@lang('site.home')</span> </a>
                  </li>
                  <li class="nav-item ">
                    <a href="{{url('/')}}#servicesss" class="nav-link" > <span class="list-item-ser "> @lang('site.our_service')</span></a>
                  </li>
                  <li class="nav-item">
                      <a href="{{url('/')}}#contact_usss" class="nav-link" ><span class="list-item-cont ">@lang('site.contact_us')</span></a>
                  </li>
                  <li class="nav-item ">
                    
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                        @if(app()->getLocale()=='en'&&$localeCode=='en')
                          <?php continue; ?>
                          <a class="nav-link language " href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                            ar
                          </a>
                          @else
                          <a class="nav-link language " href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                            @if(app()->getLocale()=='ar')
                              en
                            @else
                              ar
                            @endif
                          </a>
                          <?php break; ?>
                          @endif

                        @endforeach
                  </li>
                </ul>
              </div>
                   
          </nav>
        </div>
        </div>
    <!-- start works -->
    <div class="container">
        <div class="works ">

                
                  <div class="header">
                    <h1>
                      {{$blog->{'title:'.app()->getLocale()} }}
                      </h1>
                  </div>
                
                             
            <div class="row">
                <div class=" header col-lg-7" style="color: white;margin-top: 7px;" >
                  @if($blog->type=='video')
                  <iframe width="560" height="315" src="https://www.youtube.com/embed/{{video_link($blog->video)}}" frameborder="0" allowfullscreen></iframe>
                  @else
                  <img src="{{url('public/upload/'.$blog->image)}}" style="height: 600px;width: 730px;">
                  @endif
                </div>
                <div class=" header col-lg-5" style="color: white;margin-top: 53px;margin-left: 112px;" >
                      {!!$blog->{'desc:'.app()->getLocale()} !!}
                </div>
              </div>
        </div>
    </div>
    @if(isset($sub_blogs) && count($sub_blogs)>0)
      <!-- start services -->
    <div class="container" id="servicesss">
        <div class="services">
            <div class="header">
                <h1>
                    @lang('site.projects')
                </h1>
            </div>

            @if(app()->getLocale()=='ar')
            <div class="row">
              @foreach($sub_blogs as $service)
                @if(!$service->video)
                <a class="col-lg-4" href="{{murl(str_replace(' ','_',$service->{'title:'.app()->getLocale()})) }}">
                    <div class="ser-div">
                        <div class="ser-back" style="height: 302px;padding-top: 232px;">{{$service->{'title:'.app()->getLocale()} }}</div>

                        <img src="{{url('public/upload/'.$service->image)}}" alt="" style="height: 300px;">
                    
                    </div>
                </a>
                @else
                <a class="col-lg-4" href="{{murl(str_replace(' ','_',$service->{'title:'.app()->getLocale()})) }}">
                    <div class="ser-div">
                        
                        <iframe width="310" height="301" src="https://www.youtube.com/embed/{{video_link($service->video)}}" frameborder="0" allowfullscreen></iframe>
                    </div>
                </a>
                @endif
              @endforeach 
            </div>
            @else
            <div class="row">
              @foreach($sub_blogs as $service)
              @if(!$service->video)
                        <a class="col-lg-4" href="{{murl(str_replace(' ','_',$service->{'title:'.app()->getLocale()})) }}">
                
                    <div class="ser-div">
                        <div class="ser-back" style="height: 302px;padding-top: 236px;">{{$service->{'title:'.app()->getLocale()} }}</div>
                            <img src="{{url('public/upload/'.$service->image)}}" alt="" style="height: 300px;">
                    </div>

                        </a>
              @else
              <a class="col-lg-4" href="{{murl(str_replace(' ','_',$service->{'title:'.app()->getLocale()})) }}">
                
                    <div class="ser-div">
                        
                            <iframe width="310" height="301" src="https://www.youtube.com/embed/{{video_link($service->video)}}" frameborder="0" allowfullscreen></iframe>
                    </div>

                        </a>
              @endif
              @endforeach 
            </div>
            @endif
        </div>
    </div>

        <!-- end services -->

    @endif

    <script  src="{{url('public/front/')}}/js/jquery.js"></script>
    <script src="{{url('public/front/')}}/js/bootstrap.min.js"></script>
    <script src="{{url('public/front/')}}/js/main.js"></script>
</body>
</html>
