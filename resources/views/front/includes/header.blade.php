<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from esnadrealestate.com/index.html by HTTrack Website Copier/3.x [XR&CO'2013], Wed, 09 Oct 2019 13:35:56 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
<meta charset="utf-8">
@if(isset($title))
<title>{{$title}}</title>
@else
<title>@lang('site.esnad')</title>
@endif
@if(isset($seo_title))
<meta name="title" content="{{$seo_title}}">
 @endif
 @if(isset($seo_desc))
 <meta name="description" content="{{$seo_desc}}">
 @endif
 @if(isset($seo_keywords))
 <meta name="keywords" content="{{$seo_keywords}}">
 @endif
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<title>@lang('site.esnad')</title>
<link rel="stylesheet" type="text/css" href="{{furl()}}/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="{{furl()}}/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="{{furl()}}/css/idea_homes._icons.css">
<link rel="stylesheet" type="text/css" href="{{furl()}}/css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="{{furl()}}/css/animate.min.css">
<link rel="stylesheet" type="text/css" href="{{furl()}}/css/settings.css">
<link rel="stylesheet" type="text/css" href="{{furl()}}/css/cubeportfolio.min.css">
<link rel="stylesheet" type="text/css" href="{{furl()}}/css/bootsnav.css">
@if(isset($range_index))
<link rel="stylesheet" type="text/css" href="{{furl()}}/css/range-Slider.min.css">
@else
<link rel="stylesheet" type="text/css" href="{{furl()}}/css/range-pages.min.html">
@endif
<link rel="stylesheet" type="text/css" href="{{furl()}}/css/style.css">
<link rel="stylesheet" type="text/css" href="{{furl()}}/css/color/color-1.css" id="color" />
<link rel="shortcut icon" href="{{furl()}}/images/short_icon.png">

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

</head>

<body>

<!-- LOADER -->
<div class="loader">
    
       <!--
    <div class="cssload-thecube">
    
       
        <div class="cssload-cube cssload-c1"></div>
        <div class="cssload-cube cssload-c2"></div>
        <div class="cssload-cube cssload-c4"></div>
        <div class="cssload-cube cssload-c3"></div>
        
       
    
    </div>
    -->
       <img src="{{furl()}}/images/123.gif" />
</div><!-- LOADER -->

<!-- BACK TO TOP -->
<a href="#" class="back-to"><i class="icon-arrow-up2"></i></a>
<!-- BACK TO TOP -->



<!--  Header Top Start  -->
 <header id="header-top_3">
  <div id="header-top">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
      <nav class="head-top">
        <ul>
          <li>
            
              <i class="icon-inbox"></i>
              <span>
                {{$base->email}}              </span>
        
          </li>
          <li>
            
              <i class="icon-mobile"></i>
              <span>
                {{$base->number1}}             </span>
            
          </li>
        </ul>
      </nav>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 text-right">
          <nav class="head-tooop">
            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
            @if(app()->getLocale()=='en'&&$localeCode=='en')
            <?php continue; ?>
            <span class="lang"><a href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">العربيه</a></span>
            @else
            <span class="lang"><a href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">@lang('site.lang')</a></span>
            <?php break; ?>
            @endif
            @endforeach
            <ul class="socials">
                            <li><a href="{{$base->facebook}}" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="{{$base->twitter}}" target="_blank"accesskey=""><i class="fa fa-twitter"></i></a></li>
                            <li><a href="{{$base->youtube}}" target="_blank"><i class="fa fa-youtube"></i></a></li>
                            <li><a href="{{$base->instgram}}" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            
                    </ul>
          </nav>
        </div>
      </div>
    </div>
  </div>
  <div id="header-bottom">
    <div class="container">
      <div class="row">
        <div class="col-md-3 hidden-xs hidden-sm"><a href="{{url('/')}}"><img src="{{url('upload/'.$base->logo)}}" alt="logo"/></a></div>
    <div class="col-md-2 col-sm-12 col-xs-12">
    </div>
    <div class="col-md-7 col-sm-12 col-xs-12">
      <div class="search-form">
        <input type="search"  placeholder="@lang('site.search')"/>
        <button type="search"><i class="icon-search2"></i></button>
      </div>
        </div>
      </div>
    </div>
  </div>
  <nav class="navbar navbar-default no-background navbar-sticky bootsnav">
    <div class="container">
      <!-- Start Header Navigation -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
        <i class="fa fa-bars"></i>
        </button>
        <a class="navbar-brand sticky_logo" href="{{url('/')}}"><img src="{{url('upload/'.$base->logo)}}" class="logo" alt=""></a>
      </div><!-- End Header Navigation -->
      <div class="collapse navbar-collapse nav_3 clearfix" id="navbar-menu">
        <ul class="nav navbar-nav navbar-center" data-in="fadeInDown" data-out="fadeOutUp"> 
          <li class="active">
            <a href="{{url('/')}}" >@lang('site.home')</a>
            
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">@lang('site.about_esnad')</a>
            <ul class="dropdown-menu ">
              @foreach($abouts as $about)
               <li><a href="{{murl($about->{'title:'.app()->getLocale()} )}}">{{$about->{'title:'.app()->getLocale()} }}</a></li>
              @endforeach
            </ul>
          </li>
          <li class="dropdown">
            <a href="services.html" class="dropdown-toggle" data-toggle="dropdown">@lang('site.service')</a>
            <ul class="dropdown-menu">
              @foreach($services as $service)
              <li><a href="{{murl($service->{'title:'.app()->getLocale()} )}}">{{$service->{'title:'.app()->getLocale()} }}</a></li>  
              @endforeach
            </ul>
          </li>
      <li><a href="{{murl('clients')}}">@lang('site.clients')</a></li>
       
      <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> @lang('site.media_center')</a>
            <ul class="dropdown-menu">
                <!--
              <li><a href="ads.html">الاعلانات</a></li>
              <li><a href="conference.html">الموتمرات</a></li> -->
              <li><a href="{{murl('videos')}}">@lang('site.tv_videos')</a></li>
              <li><a href="{{murl('news')}}">@lang('site.news')</a></li>
            </ul>
          </li>
          <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown"> @lang('site.humans')</a>
                  <ul class="dropdown-menu">
                    @foreach($jobs as $job)
                      <li><a href="{{murl($job->{'title:'.app()->getLocale()} )}}">{{$job->{'title:'.app()->getLocale()} }}</a></li>
                    @endforeach
                      <li><a href="jobs.html">التوظيف</a></li>
                  </ul>    
     <!-- <li><a href="contactus.php">اتصل بنا</a></li>-->
       
        </ul>
      </div>
    </div>
  </nav>
</header><!--  Header Top End  -->
