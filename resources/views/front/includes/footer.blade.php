<!-- CONTACT -->
<section id="contact" class="bg-color-red">
  <div class="container">
         <div class="row">
      <div class="col-md-4 col-sm-4 col-xs-12 text-center">
        <div class="get-tuch">
          <i class="icon-telephone114"></i>
          <ul>
            <li><h4>@lang('site.phone_number')</h4></li>
            <li><p>{{$base->number2}}</p></li>
          </ul>
        </div>
      </div>
      <div class="col-md-4 col-sm-4 col-xs-12 text-center">
        <div class="get-tuch">
          <i class="icon-icons74"></i>
          <ul>
            <li>
              <h4>@lang('site.address')</h4>
            </li>
            <li><p>{{$base->{'address:'.app()->getLocale()} }}</p></li>
          </ul>
        </div>
      </div>
      <div class="col-md-4 col-sm-4 col-xs-12 text-center">
        <div class="get-tuch">
          <i class="icon-icons142"></i>
          <ul>
            <li><h4 class="p-font-17">@lang('site.email')</h4></li>
            <li><a href="#."><p>{{$base->email}}</p></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- #/CONTACT -->
<!-- FOOTER -->
<footer id="footer" class="footer divider layer-overlay overlay-dark-8">
 
  <div class="footer-bottom bg-black-333">
    <div class="container">
    <div class="row border-bottom">
      <div class="col-sm-6 col-md-3">
        <div class="widget dark">
          <h4 class="widget-title">@lang('site.about_esnad')</h4>
          <div class="small-title">
            <div class="line1 background-color-white"></div>
            <div class="line2 background-color-white"></div>
            <div class="clearfix"></div>
          </div>
          <ul class="list angle-double-right list-border">
              @foreach($abouts as $about)
               <li><a href="{{murl($about->{'title:'.app()->getLocale()} )}}">{{$about->{'title:'.app()->getLocale()} }}</a></li>
              @endforeach
          </ul>
        </div>
      </div>
      <div class="col-sm-6 col-md-3">
        <div class="widget dark">
          <h4 class="widget-title">@lang('site.service')</h4>
          <div class="small-title">
            <div class="line1 background-color-white"></div>
            <div class="line2 background-color-white"></div>
            <div class="clearfix"></div>
          </div>
          <ul class="list angle-double-right list-border">
              @foreach($services as $service)
              <li><a href="{{murl($service->{'title:'.app()->getLocale()} )}}">{{$service->{'title:'.app()->getLocale()} }}</a></li>  
              @endforeach
          </ul>
        </div>
      </div>
      <div class="col-sm-6 col-md-3">
        <div class="widget dark">
          <h4 class="widget-title ">@lang('site.courses')</h4>
          <div class="small-title">
            <div class="line1 background-color-white"></div>
            <div class="line2 background-color-white"></div>
            <div class="clearfix"></div>
          </div>
          <ul class="list list-border">
                     
          </ul>
        </div>
      </div>
      <div class="col-sm-6 col-md-3">
        <div class="widget dark">
          <h4 class="widget-title ">@lang('site.media_center')</h4>
          <div class="small-title">
            <div class="line1 background-color-white"></div>
            <div class="line2 background-color-white"></div>
            <div class="clearfix"></div>
          </div>
          <ul class="list list-border">
              <li><a href="{{murl('videos')}}">@lang('site.tv_videos')</a></li>
              <li><a href="{{murl('news')}}">@lang('site.news')</a></li>           
          </ul>
        </div>
      </div>
   </div>
    
      <div class="row">
        <div class="col-md-6 col-sm-5">
          <p class="font-11 text-black-777 m-0 copy-right">حقوق النشر؛ 2016 <a href="#." ><span class="color_redr"> إسناد </span></a>. كل الحقوق محفوظة 
          <a href="#"></a></p>
        </div>
        <div class="col-md-6 col-sm-7 text-right">
          <div class="widget no-border m-0">
            <ul class="list-inline sm-text-center mt-5 font-12">
              <li> <a href="{{murl('/')}}" >@lang('site.home')</a> </li>
              <li>|</li>
              <li> <a href="{{murl($abouts->first()->{'title:'.app()->getLocale()} )}}" >@lang('site.about_us')</a></li>
              <li>|</li>
              <li> <a href="{{murl('contact-us')}}">@lang('site.contact_us')</a> </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- #/FOOTER -->


<script src="{{furl()}}/js/jquery.2.2.3.min.js"></script>
<script src="{{furl()}}/js/bootstrap.min.js"></script>
<script src="{{furl()}}/js/jquery.appear.js"></script>
<script src="{{furl()}}/js/modernizr.html"></script>
<script src="{{furl()}}/js/jquery.parallax-1.1.3.js"></script>
<script src="{{furl()}}/js/owl.carousel.min.js"></script>
<script src="{{furl()}}/js/jquery.fancybox.js"></script>
<script src="{{furl()}}/js/cubeportfolio.min.js"></script>
<script src="{{furl()}}/js/range-Slider.min.js"></script>
<script src="{{furl()}}/js/selectbox-0.2.min.js"></script>
<script src="{{furl()}}/js/bootsnav.js"></script>
<script src="{{furl()}}/js/zelect.js"></script>
<script src="{{furl()}}/js/themepunch/jquery.themepunch.tools.min.js"></script>
<script src="{{furl()}}/js/themepunch/jquery.themepunch.revolution.min.js"></script>
<script src="{{furl()}}/js/themepunch/revolution.extension.layeranimation.min.js"></script>
<script src="{{furl()}}/js/themepunch/revolution.extension.navigation.min.js"></script>
<script src="{{furl()}}/js/themepunch/revolution.extension.parallax.min.js"></script>
<script src="{{furl()}}/js/themepunch/revolution.extension.slideanims.min.js"></script>
<script src="{{furl()}}/js/themepunch/revolution.extension.video.min.js"></script>
<script src="{{furl()}}/js/functions.js"></script>
<script>
$('#myModal').on('shown.bs.modal', function () {
  $('#myInput').focus()
})
</script>
@include('front.sweetalert')
</body>


<!-- Mirrored from esnadrealestate.com/index.html by HTTrack Website Copier/3.x [XR&CO'2013], Wed, 09 Oct 2019 13:41:15 GMT -->
</html>