
@if (session('front_success'))

    <script>
        Swal.fire({
				  title: '{{trans("site.success")}}',
				  text: "{{session('front_success')}}",
				  type: 'success',
				  showCancelButton: false,
				  confirmButtonColor: '#3085d6',
				  confirmButtonText: '{{trans("site.ok")}}'
				})
    </script>

@endif

