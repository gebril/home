@extends('front.master')
@section('content')
</head>
<body id="others">
    <div class="vis_page">
        <header>
            @include('front.includes.nav')
        </header>
        <div class="vis-xs-12 vis-sm-12 vis-md-12 vis-lg-12">
            <img class="vis-404-img wow fadeInDown" src="{{asset('public/front/img/404.png')}}" alt="لم يتم العثور على الصفحة">
        </div>
        <div class="vis-xs-12 vis-sm-12 vis-md-12 vis-lg-12 text-center">
            <p class="wow fadeInDown">عذراً... لم يتم العثور على الصفحة</p>
            <a class="vis-home-btn wow fadeInUp" href="{{url('index')}}">الرجوع للصفحة الرئيسية</a>
        </div>
@endsection