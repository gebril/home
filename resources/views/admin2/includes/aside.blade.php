<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
				<!-- BEGIN: Left Aside -->
				<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
					<i class="la la-close"></i>
				</button>
				<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
					<!-- BEGIN: Aside Menu -->
	<div id="m_ver_menu" 
		class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " 
		data-menu-vertical="true"
		 data-menu-scrollable="false" data-menu-dropdown-timeout="500"  
		>
						<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
							<li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
								<a class="m-menu__link ">
									<i class="m-menu__link-icon flaticon-line-graph"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">
												@lang('admin.dashboard')
											</span>
											
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__section">
								<h4 class="m-menu__section-text">
									
								</h4>
								<i class="m-menu__section-icon flaticon-more-v3"></i>
							</li>
							
<!-- 							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="{{aurl('index/sliders')}}" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-more-v4"></i>
									<span class="m-menu__link-text" style="padding-left:5px;">
										@lang('admin.slider')
									</span>
								</a>
							</li> -->
							{{-- <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="{{aurl('index/about-us')}}" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-layers"></i>
									<span class="m-menu__link-text" style="padding-left:5px;">
										@lang('admin.about_us')
									</span>
								</a>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="{{aurl('contactus-seo')}}" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-chat-1"></i>
									<span class="m-menu__link-text" style="padding-left:5px;">
										@lang('admin.contact_us_seo')
									</span>
								</a>
							</li> --}}
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="{{aurl('index/base-data')}}" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-squares-2"></i>
									<span class="m-menu__link-text" style="padding-left:5px;">
										@lang('admin.base_data')
									</span>
								</a>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="{{aurl('blogs/service')}}" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-folder-2"></i>
									<span class="m-menu__link-text" style="padding-left:5px;">
										@lang('admin.services')
									</span>
								</a>
							</li>
							<!-- <li class="m-menu__item " aria-haspopup="true" >
								<a  href="{{aurl('tv-videos')}}" class="m-menu__link ">
									<i class="m-menu__link-icon flaticon-imac"></i>
									<span class="m-menu__link-text" style="padding-left:5px;">
										@lang('admin.tv_meetings')
									</span>
								</a>
							</li>
							<li class="m-menu__item " aria-haspopup="true" >
								<a  href="{{aurl('blogs/projects')}}" class="m-menu__link ">
									<i class="m-menu__link-icon flaticon-statistics"></i>
									<span class="m-menu__link-text" style="padding-left:5px;">
										@lang('admin.projects')
									</span>
								</a>
							</li> -->
							
							
							<!-- <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover"> style="padding-left:5px;"
								<a  href="#" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-layers"></i>
									<span class="m-menu__link-text">
										@lang('admin.clients')
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
											<a  href="{{aurl('clients')}}" class="m-menu__link m-menu__toggle">
												<i class="m-menu__link-icon flaticon-layers"></i>
												<span class="m-menu__link-text">
													@lang('admin.clients')
												</span>
											</a>
										</li>
										<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
											<a  href="{{aurl('clients-seo')}}" class="m-menu__link m-menu__toggle">
												<i class="m-menu__link-icon flaticon-layers"></i>
												<span class="m-menu__link-text">
													@lang('admin.clients_seo')
												</span>
											</a>
										</li>								
									</ul>
								</div>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover"> style="padding-left:5px;"
								<a  href="#" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-layers"></i>
									<span class="m-menu__link-text">
										@lang('admin.media_center')
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item " aria-haspopup="true" >
												<a  href="{{aurl('blogs/news')}}" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													@lang('admin.news')
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
												<a  href="{{aurl('news-seo')}}" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													@lang('admin.news_seo')
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="{{aurl('tv-videos')}}" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													@lang('admin.tv_meetings')
												</span>
											</a>
										</li>	
									</ul>
								</div>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover"> style="padding-left:5px;"
								<a  href="#" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-layers"></i>
									<span class="m-menu__link-text">
										@lang('admin.projects')
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu">
									<span class="m-menu__arrow"></span>
										<ul class="m-menu__subnav">
											<li class="m-menu__item " aria-haspopup="true" >
												<a  href="{{aurl('blogs/projects')}}" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													@lang('admin.blogs')
												</span>
											</a>
										</li>	
									</ul>
								</div>
							</li>	
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover"> style="padding-left:5px;"
								<a  href="{{aurl('cvs')}}" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-layers"></i>
									<span class="m-menu__link-text">
										@lang('admin.cv_jobs')
									</span>
								</a>
							</li> -->						
						</ul>
					</div>
					<!-- END: Aside Menu -->
				</div>
				<!-- END: Left Aside -->
				
			