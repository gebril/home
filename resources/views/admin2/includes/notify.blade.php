@if (session('success'))

    <script>
        Swal.fire({
              title: '{{trans("admin.success")}}',
              html: '{{ session("success") }}',
              type: 'success',
               showCancelButton: false,
              confirmButtonColor: '#3085d6',
               confirmButtonText: '{{trans("admin.ok")}}'
           });

    </script>

@endif
@if (session('error'))

    <script>
        Swal.fire({
              title: '{{trans("admin.error")}}',
              html: '{{ session("error") }}',
              type: 'error',
               showCancelButton: false,
              confirmButtonColor: '#3085d6',
               confirmButtonText: '{{trans("admin.ok")}}'
           });

    </script>

@endif
