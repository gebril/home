@extends('admin2.index')
@section('content')

<div class="row">

	<div class="col-md-12">
		<!--begin::Portlet-->
		<div class="m-portlet m-portlet--tab">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon m--hide">
							<i class="la la-gear"></i>
						</span>
						<h3 style="margin-top: 20px;margin-right: 20px;">
								@lang('admin.create slider')
						</h3>
					</div>
				</div>
			</div>
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right" action="{{aurl('index/store-slider')}}" method="post" enctype="multipart/form-data"> 
				{{csrf_field()}}
				<div class="m-portlet__body">
					<ul class="nav nav-pills nav-fill" role="tablist">
						@foreach(config('translatable.locales') as $locale)
							<?php 
							$expanded='';
							if($locale=='ar' && app()->getLocale() == 'ar'){
										$expanded='active';
									}
							if($locale=='en' && app()->getLocale() == 'en'){
										$expanded='active';
									}
										?>
						<li class="nav-item">
							<a class="nav-link {{$expanded}}" data-toggle="tab" href="#m_tabs_our_plus_5_{{$locale}}" aria-expanded="true">
							@if($locale == 'ar')
								@lang('admin.arabic')
							@else
								@lang('admin.english')
							@endif
							</a>
						</li>
						@endforeach
					</ul>
													
					<div class="tab-content">
						@foreach(config('translatable.locales') as $locale)					<?php 
							$expanded='';
							if($locale=='ar' && app()->getLocale() == 'ar'){
										$expanded='active';
									}
							if($locale=='en' && app()->getLocale() == 'en'){
										$expanded='active';
									}
										?>	
						<div class="tab-pane {{$expanded}}" id="m_tabs_our_plus_5_{{$locale}}" role="tabpanel" aria-expanded="{{$expanded}}">
							<div class="m-portlet__body">
								<!-- Section one -->
								<div class="form-group m-form__group">
									<label for="exampleInputEmail1">
										 @lang('admin.title')
									</label>
									<input required="required" type="text" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" name="{{$locale}}[title]">
								</div>
								<div class="form-group m-form__group">
									<label for="exampleInputEmail1">
										 @lang('admin.sub_title')
									</label>
									<input required="required" type="text" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" name="{{$locale}}[sub_title]"  value="">	
								</div>
								<!-- Description -->
								<div class="form-group m-form__group">
									<label for="exampleTextarea">
										@lang('admin.desc') 
									</label>
									<textarea required="required" class="form-control m-input" id="exampleTextarea" rows="3" name="{{$locale}}[desc]"></textarea>
									<script type="text/javascript">	
									CKEDITOR.replace('{{$locale}}[desc]')
									</script>
								</div>
								<!-- SEO Columns -->
								<h3 style="margin-top: 20px;">
										@lang('admin.seocolumns')
									</h3>
								<div class="form-group m-form__group">
									<label for="exampleInputEmail1">
										@lang('admin.title')
									</label>
									<div></div>
									<label class="custom-file">
										<input required="required" type="text"  class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp" name="{{$locale}}[seo_title]" value="">
									</label>
								</div>
								<div class="form-group m-form__group">
									<label for="exampleInputEmail1">
										@lang('admin.desc')
									</label>
									<textarea  required="required" type="text" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp"  name="{{$locale}}[seo_desc]"></textarea>
								</div>
								<div class="form-group m-form__group">
									<label for="exampleInputEmail1">
										@lang('admin.key_words')
									</label>
									<textarea  required="required" type="text" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp"  name="{{$locale}}[key_words]"></textarea>
								</div>	
								<!-- Image -->
								<div class="form-group m-form__group">
									<label for="exampleInputEmail1">
										@lang('admin.images')	 
									</label>
									<div></div>
									<label class="custom-file">
										<input type="file" required="required" id="slider_files_{{$locale}}" class="custom-file-input image" name="image_{{$locale}}[]"  multiple>
										<span class="custom-file-control"></span>
									</label>
								</div>
								<div id="image_preview_{{$locale}}"></div>		
							</div>			
						</div>
							@endforeach
						<hr>
					</div>
					<div class="m-portlet__foot m-portlet__foot--fit" >
						<div class="m-form__actions" style="margin-right: 450px;">
							<button type="Submit" class="btn btn-primary">@lang('admin.savechanges')
							</button>
						</div>
					</div>
				</div>
			</form>
									<!--end::Form-->
		</div>
								<!--end::Portlet-->
	</div>

	
</div>

@endsection