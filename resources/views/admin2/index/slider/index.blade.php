@extends('admin2.index')
@section('content')
<div class="row">
	<div class="col-md-12" style="margin-top: 100px;">
		<div class="panel panel-default" >
			<div class="panel-heading">
				<h1 style="margin-bottom: 80px;"></h1>
					
					<a href="{{aurl('index/create-slider')}}" class="btn btn-primary pull-right" style="margin-left: 15px;">@lang('admin.Add New')</a>
				
			</div>
			<div class="panel-body" style="">
				<table class="m-datatable__table" id="sliders-table" style="display: block;">
					<thead class="m-datatable__head">
						<tr class="m-datatable__row" style="height: 53px;">
							<th>#</th>
							<th ><span style="width: 70px; margin-right: 50px;">@lang('admin.title') </span></th>
							<th ><span style="width: 70px; margin-right: 50px;">@lang('admin.sub_title') </span></th>
							<th ><span style="width: 70px; margin-right: 50px;">@lang('admin.Delete')</span></th>
							<th ><span style="width: 70px; margin-right: 50px;">@lang('admin.Edit')</span></th>
						</tr>
					</thead>
					
				</table>
			</div>
		</div>		
	</div>
</div>
@include('admin2.modals.blog')
@endsection