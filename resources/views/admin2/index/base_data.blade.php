@extends('admin2.index')
@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
												@lang('admin.base_data')
						</h3>
					</div>  
				</div>
			</div>
			<div class="m-portlet__body">
				<!--begin::Section-->
				<div class="m-section">
					<div class="m-section__content">
						<div class="m-portlet__body">
							<ul class="nav nav-pills nav-fill" role="tablist">
								@foreach(config('translatable.locales') as $locale)
									<?php
									$expanded='';
									if($locale=='ar' && app()->getLocale() == 'ar'){
												$expanded='active';
											}
									if($locale=='en' && app()->getLocale() == 'en'){
												$expanded='active';
											}
												?>
								<li class="nav-item">
									<a class="nav-link {{$expanded}}" data-toggle="tab" href="#m_tabs_our_plus_4_{{$locale}}" aria-expanded="true">
									@if($locale == 'ar')
										@lang('admin.arabic')
									@else
										@lang('admin.english')
									@endif
									</a>
								</li>
								@endforeach
							</ul>
							<form class="m-form m-form--fit m-form--label-align-right" action="{{aurl('base-data/update')}}" method="post" enctype="multipart/form-data">
								{{csrf_field()}}
								<div class="tab-content">
									@foreach(config('translatable.locales') as $locale)					<?php
										$expanded='';
										if($locale=='ar' && app()->getLocale() == 'ar'){
													$expanded='active';
												}
										if($locale=='en' && app()->getLocale() == 'en'){
													$expanded='active';
												}
													?>
									<div class="tab-pane {{$expanded}}" id="m_tabs_our_plus_4_{{$locale}}" role="tabpanel" aria-expanded="{{$expanded}}">
										<div class="m-portlet__body">
											<!-- Section one -->
											{{-- <div class="form-group m-form__group">
												<label for="exampleInputEmail1">
													@lang('admin.address')
												</label>
												<input required="required" type="text" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp"  name="{{$locale}}[address]" value="{{$about->{'address:'.$locale} }}">
											</div>
											<!-- Section one -->
											<div class="form-group m-form__group">
												<label for="exampleInputEmail1">
													@lang('admin.vision1')
												</label>
												<textarea  required="required" type="text" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp"  name="{{$locale}}[vision]">{{$about->{'vision:'.$locale} }}</textarea>
												<script>
								                        CKEDITOR.replace( '{{$locale}}[vision]' );
								                </script>
											</div>
											<div class="form-group m-form__group">
												<label for="exampleInputEmail1">
													@lang('admin.message')
												</label>
												<textarea  required="required" type="text" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp"  name="{{$locale}}[message]">{{$about->{'message:'.$locale} }}</textarea>
											</div>
											<script>
								                        CKEDITOR.replace( '{{$locale}}[message]' );
								                </script>
											<div class="form-group m-form__group">
												<label for="exampleInputEmail1">
													@lang('admin.founder_word')
												</label>
												<textarea  required="required" type="text" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp"  name="{{$locale}}[founder]">{{$about->{'founder:'.$locale} }}</textarea>
											</div>
											<script>
								                        CKEDITOR.replace( '{{$locale}}[founder]' );
								                </script> --}}
											<!-- SEO Columns -->
											<h3 style="margin-top: 20px;">
													@lang('admin.seocolumns')
												</h3>
											<div class="form-group m-form__group">
												<label for="exampleInputEmail1">
													@lang('admin.title')
												</label>
												<div></div>
												<label class="custom-file">
													<input required="required" type="text"  class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp" name="{{$locale}}[seo_title]" value="{{$about->{'seo_title:'.$locale} }}">
												</label>
											</div>
											<div class="form-group m-form__group">
												<label for="exampleInputEmail1">
													@lang('admin.desc')
												</label>
												<textarea  required="required" type="text" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp"  name="{{$locale}}[seo_desc]">{{$about->{'seo_desc:'.$locale} }}</textarea>
											</div>
											<div class="form-group m-form__group">
												<label for="exampleInputEmail1">
													@lang('admin.key_words')
												</label>
												<textarea  required="required" type="text" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp"  name="{{$locale}}[key_words]">{{$about->{'key_words:'.$locale} }}</textarea>
											</div>
										</div>
									</div>
									@endforeach
									<!-- Address -->
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											@lang('admin.email')
										</label>
										<div></div>
										<label class="custom-file">
											<input required="required" type="text"  class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp" name="email" value="{{$about->email}}">
										</label>
									</div>
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											@lang('admin.number1')
										</label>
										<div></div>
										<label class="custom-file">
											<input required="required" type="text"  class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp" name="number1" value="{{$about->number1}}">
										</label>
									</div>
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											@lang('admin.number2')
										</label>
										<div></div>
										<label class="custom-file">
											<input required="required" type="text"  class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp" name="number2" value="{{$about->number2}}">
										</label>
									</div>
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											@lang('admin.url')
										</label>
										<div></div>
										<label class="custom-file">
											<input required="required" type="text"  class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp" name="url" value="{{$about->url}}">
										</label>
									</div>
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											@lang('admin.facebook')
										</label>
										<div></div>
										<label class="custom-file">
											<input required="required" type="text"  class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp" name="facebook" value="{{$about->facebook}}">
										</label>
									</div>
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											@lang('admin.twitter')
										</label>
										<div></div>
										<label class="custom-file">
											<input required="required" type="text"  class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp" name="twitter" value="{{$about->twitter}}">
										</label>
									</div>
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											@lang('admin.instgram')
										</label>
										<div></div>
										<label class="custom-file">
											<input required="required" type="text"  class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp" name="instgram" value="{{$about->instgram}}">
										</label>
									</div>
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											@lang('admin.youtube')
										</label>
										<div></div>
										<label class="custom-file">
											<input required="required" type="text"  class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp" name="youtube" value="{{$about->youtube}}">
										</label>
									</div>
									<!-- Save -->
									<div class="m-portlet__foot m-portlet__foot--fit" >
												<div class="m-form__actions" style="margin-right: 450px;">
													<button type="Submit" class="btn btn-info">@lang('admin.savechanges')
													</button>
												</div>
											</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!--end::Section-->
			</div>
			<!--end::Form-->
		</div>
	</div>
</div>
@endsection
