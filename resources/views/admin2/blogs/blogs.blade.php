@extends('admin2.index')
@section('content')
@if (count($errors) > 0)
      @foreach ($errors->all() as $item)
        <p class="alert alert-danger">{{$item}}</p>
      @endforeach
    @endif
<div class="row">
	<div class="col-md-12">
		<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							@if($type=='projects')
												@lang('admin.projects')
							@elseif($type=='video')
												@lang('admin.video')
							@else
												@lang('admin.blogs')
							@endif
						</h3>
					</div>
				</div>
			</div>
			<div class="m-portlet__body">
				<!--begin::Section-->
				<div class="m-section">
					<div class="m-section__content">
							<table class="table table-striped m-table">
								<thead>
									<tr>
										<th>
											#
										</th>
										<th>
											@lang('admin.title')
										</th>
										@if($type=='service')
										<th>
											@lang('admin.add_projects')
										</th>
										<th>
											@lang('admin.videos')
										</th>
										@endif
										<th>
											@lang('admin.delete')
										</th>
										<th>
											@lang('admin.edit')
										</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=0;?>
									@foreach($blogs as $blog)
									<?php $i++; ?>
									<tr>
										<th scope="row" style="direction: rtl;">
											{{$i}}
										</th>
										<td style="direction: rtl;">
											{{$blog->{'title:' . app()->getLocale()} }}
										</td>
										@if($type=='service')
										<td style="direction: rtl;">
											<a class="btn m-btn--pill m-btn--air btn-primary" href="{{aurl('blog-projects/projects/'.$blog->id)}}" style="background-color: cadetblue;"><i class="fa fa-plus" ></i> 
               								 </a>
										</td>
										<td style="direction: rtl;">
											<a class="btn m-btn--pill m-btn--air btn-primary" href="{{aurl('blog-projects/video/'.$blog->id)}}" style="background-color: cadetblue;"><i class="fa fa-plus" ></i> 
               								 </a>
										</td>
										@endif
										<td style="direction: rtl;">
											<button type="button" class="btn m-btn--pill m-btn--air         btn-danger" id="delete-blog" data-toggle="modal" data-target="#blog_delete">
												<i class="fa fa-trash-o"></i>
												<input required="required" type="hidden" name="id" id="blog-id" value="{{$blog->id}}">
											</button>
										</td>
										<td style="direction: rtl;">
											<a class="btn m-btn--pill m-btn--air btn-info" href="{{aurl('edit-blog/'.$blog->id)}}"><i class="fa fa-edit" ></i> 
               								 </a>
										</td>
									</tr>
									@endforeach
							</tbody>
						</table>
						<hr>

						<div class="m-portlet__body">
							<ul class="nav nav-pills nav-fill" role="tablist">
								@foreach(config('translatable.locales') as $locale)
									<?php 
									$expanded='';
									if($locale=='ar' && app()->getLocale() == 'ar'){
												$expanded='active';
											}
									if($locale=='en' && app()->getLocale() == 'en'){
												$expanded='active';
											}
												?>
								<li class="nav-item">
									<a class="nav-link {{$expanded}}" data-toggle="tab" href="#m_tabs_our_plus_4_{{$locale}}" aria-expanded="true">
									@if($locale == 'ar')
										@lang('admin.arabic')
									@else
										@lang('admin.english')
									@endif
									</a>
								</li>
								@endforeach
							</ul>
						
						@if($type=='video')
								<form class="m-form m-form--fit m-form--label-align-right" action="{{aurl('blog-projects/'.$id)}}" method="post" enctype="multipart/form-data">
									{{csrf_field()}}						
									<div class="tab-content">
										@foreach(config('translatable.locales') as $locale)					<?php 
											$expanded='';
											if($locale=='ar' && app()->getLocale() == 'ar'){
														$expanded='active';
													}
											if($locale=='en' && app()->getLocale() == 'en'){
														$expanded='active';
													}
														?>	
										<div class="tab-pane {{$expanded}}" id="m_tabs_our_plus_4_{{$locale}}" role="tabpanel" aria-expanded="{{$expanded}}">
											<div class="m-portlet__body">				
												<!-- Section one -->
												<div class="form-group m-form__group">
													<label for="exampleInputEmail1">
														@lang('admin.video_title')
													</label>
													<input required="required" type="text" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" name="{{$locale}}[title]">	
												</div>
											</div>			
										</div>			
										@endforeach
										<hr>
										<!-- Image -->
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												@lang('admin.video_link')
											</label>
											<input required="required" type="text" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" name="video">
											<input type="hidden" name="type" value="video">	
										</div>
										<div class="m-portlet__foot m-portlet__foot--fit" >
											<div class="m-form__actions" style="margin-right: 450px;">
												<button type="Submit" class="btn btn-info">@lang('admin.savechanges')
												</button>
											</div>
										</div>
									</div>
								</form>
							
						@else
							<form class="m-form m-form--fit m-form--label-align-right" action="{{aurl('blog/add')}}" method="post" enctype="multipart/form-data">
								{{csrf_field()}}						
								<div class="tab-content">
									@foreach(config('translatable.locales') as $locale)					<?php 
										$expanded='';
										if($locale=='ar' && app()->getLocale() == 'ar'){
													$expanded='active';
												}
										if($locale=='en' && app()->getLocale() == 'en'){
													$expanded='active';
												}
													?>	
									<div class="tab-pane {{$expanded}}" id="m_tabs_our_plus_4_{{$locale}}" role="tabpanel" aria-expanded="{{$expanded}}">
										<div class="m-portlet__body">				
											<!-- Section one -->
											<div class="form-group m-form__group">
												<label for="exampleInputEmail1">
													@lang('admin.title')
												</label>
												<input required="required" type="text" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp"  name="{{$locale}}[title]">	
											</div>
											<!-- Description -->
											<div class="form-group m-form__group">
												<label for="exampleInputEmail1">
													@lang('admin.desc')
												</label>
												<textarea  required="required" type="text" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp"  name="{{$locale}}[desc]"></textarea>
												<script>
								                        CKEDITOR.replace( '{{$locale}}[desc]' );
								                </script>
											</div>
											
											<!-- SEO Columns -->
											<h3 style="margin-top: 20px;">
													@lang('admin.seocolumns')
												</h3>
											<div class="form-group m-form__group">
												<label for="exampleInputEmail1">
													@lang('admin.title')
												</label>
												<div></div>
												<label class="custom-file">
													<input required="required" type="text"  class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp" name="{{$locale}}[seo_title]">
												</label>
											</div>
											<div class="form-group m-form__group">
												<label for="exampleInputEmail1">
													@lang('admin.desc')
												</label>
												<textarea  required="required" type="text" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp"  name="{{$locale}}[seo_desc]"></textarea>
											</div>
											<div class="form-group m-form__group">
												<label for="exampleInputEmail1">
													@lang('admin.key_words')
												</label>
												<textarea  required="required" type="text" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp"  name="{{$locale}}[key_words]"></textarea>
											</div>		
										</div>			
									</div>			
									@endforeach
									@if($type=='projects')
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											@lang('admin.project_link')
										</label>
										<input required="required" type="text" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" name="link">
										<input type="hidden" name="type" value="projects">
										<input type="hidden" name="parent_id" value="{{$id}}">
									</div>
									@endif
									<!-- Image -->
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											@lang('admin.blogimage') 
										</label>
										<div></div>
										<label class="custom-file">
											<input  type="file" id="file2" class="custom-file-input image" name="image">
											<span class="custom-file-control"></span>
										</label>
									</div>
									<img src="" class="image-preview" style="width:100px;margin-right: 19px;">
									<div class="m-portlet__foot m-portlet__foot--fit" >
												<div class="m-form__actions" style="margin-right: 450px;">
													<button type="Submit" class="btn btn-info">@lang('admin.savechanges')
													</button>
												</div>
												<input type="hidden" name="type" value="{{$type}}">
											</div>
								</div>
							</form>
							@endif
						</div>
					</div>
				</div>
				<!--end::Section-->
			</div>
			<!--end::Form-->
		</div>
	</div>
</div>
@include('admin2.modals.blog')
@endsection