@extends('admin2.index')
@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							@lang('admin.categories')
						</h3>
					</div>
				</div>
			</div>
			<div class="m-portlet__body">
				<!--begin::Section-->
				<div class="m-section">
					<div class="m-section__content">
							<table class="table table-striped m-table">
								<thead>
									<tr>
										<th>
											#
										</th>
										<th>
											@lang('admin.content')
										</th>
										<th>
											@lang('admin.delete')
										</th>
										<th>
											@lang('admin.edit')
										</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=0;?>
									@foreach($cats as $cat)
									<?php $i++; ?>
									<tr>
										<th scope="row">
											{{$i}}
										</th>
										<td>
											{{$cat->{'content:' . app()->getLocale()} }}
										</td>
										
										<td>
											<button type="button" class="btn m-btn--pill m-btn--air         btn-danger" id="delete-blogcat" data-toggle="modal" data-target="#cat_delete">
												<i class="fa fa-trash-o"></i>
												<input required="required" type="hidden" name="id" id="blogcat-id" value="{{$cat->id}}">
											</button>
										</td>
										<td>
											<button type="button" class="btn m-btn--pill m-btn--air         btn-info" id="edit-blogcat" data-toggle="modal" data-target="#categories_updates">
												<i class="fa fa-edit"></i>
												<input required="required" type="hidden" name="id" id="blogcat-id" value="{{$cat->id}}">
												<input required="required" type="hidden" name="content" id="blogcat-content-en" value="{{$cat->{'content:en'} }}">
												<input required="required" type="hidden" name="content" id="blogcat-content-ar" value="{{$cat->{'content:ar'} }}">
											</button>
										</td>
									</tr>
									@endforeach
							</tbody>
						</table>
						<hr>
						<div class="m-portlet__body">
							<ul class="nav nav-pills nav-fill" role="tablist">
								@foreach(config('translatable.locales') as $locale)
									<?php 
									$expanded='';
									if($locale=='ar' && app()->getLocale() == 'ar'){
												$expanded='active';
											}
									if($locale=='en' && app()->getLocale() == 'en'){
												$expanded='active';
											}
												?>
								<li class="nav-item">
									<a class="nav-link {{$expanded}}" data-toggle="tab" href="#m_tabs_our_plus_4_{{$locale}}" aria-expanded="true">
									@if($locale == 'ar')
										@lang('admin.arabic')
									@else
										@lang('admin.english')
									@endif
									</a>
								</li>
								@endforeach
							</ul>
							<form class="m-form m-form--fit m-form--label-align-right" action="{{aurl('Add-blogcategory')}}" method="post">
								{{csrf_field()}}				
								<div class="tab-content">
									@foreach(config('translatable.locales') as $locale)					<?php 
										$expanded='';
										if($locale=='ar' && app()->getLocale() == 'ar'){
													$expanded='active';
												}
										if($locale=='en' && app()->getLocale() == 'en'){
													$expanded='active';
												}
													?>	
									<div class="tab-pane {{$expanded}}" id="m_tabs_our_plus_4_{{$locale}}" role="tabpanel" aria-expanded="{{$expanded}}">
										<div class="m-portlet__body">				
											<!-- Section one -->
											<div class="form-group m-form__group">
												<label for="exampleInputEmail1">
													 @lang('admin.content')@lang('admin.in') @if($locale == 'ar')
														@lang('admin.arabic')
													@else
														@lang('admin.english')
													@endif
												</label>
												<input required="required" type="text" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" name="{{$locale}}[content]">	
											</div>			
										</div>			
									</div>
									@endforeach
									<div class="m-portlet__foot m-portlet__foot--fit" >
										<div class="m-form__actions" style="margin-right: 450px;">
											<button type="Submit" class="btn m-btn--pill m-btn--air btn btn-info">         @lang('admin.savechanges')
											</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!--end::Section-->
			</div>
			<!--end::Form-->
		</div>
	</div>
</div>
@include('admin2.modals.blog')
@endsection