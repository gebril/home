@extends('admin2.index')
@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
												@lang('admin.blogs')
						</h3>
					</div>
				</div>
			</div>
			<div class="m-portlet__body">
				<!--begin::Section-->
				<div class="m-section">
					<div class="m-section__content">
						<div class="m-portlet__body">
							<ul class="nav nav-pills nav-fill" role="tablist">
								@foreach(config('translatable.locales') as $locale)
									<?php 
									$expanded='';
									if($locale=='ar' && app()->getLocale() == 'ar'){
												$expanded='active';
											}
									if($locale=='en' && app()->getLocale() == 'en'){
												$expanded='active';
											}
												?>
								<li class="nav-item">
									<a class="nav-link {{$expanded}}" data-toggle="tab" href="#m_tabs_our_plus_4_{{$locale}}" aria-expanded="true">
									@if($locale == 'ar')
										@lang('admin.arabic')
									@else
										@lang('admin.english')
									@endif
									</a>
								</li>
								@endforeach
							</ul>
							@if($blog->type=='video')
								<form class="m-form m-form--fit m-form--label-align-right" action="{{aurl('blog-update/'.$blog->id)}}" method="post" enctype="multipart/form-data">
									{{csrf_field()}}
									@method('put')						
									<div class="tab-content">
										@foreach(config('translatable.locales') as $locale)					<?php 
											$expanded='';
											if($locale=='ar' && app()->getLocale() == 'ar'){
														$expanded='active';
													}
											if($locale=='en' && app()->getLocale() == 'en'){
														$expanded='active';
													}
														?>	
										<div class="tab-pane {{$expanded}}" id="m_tabs_our_plus_4_{{$locale}}" role="tabpanel" aria-expanded="{{$expanded}}">
											<div class="m-portlet__body">				
												<!-- Section one -->
												<div class="form-group m-form__group">
													<label for="exampleInputEmail1">
														@lang('admin.video_title')
													</label>
													<input required="required" type="text" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" name="{{$locale}}[title]" value="{{$blog->{'title:'.$locale} }}">	
												</div>
											</div>			
										</div>			
										@endforeach
										<hr>
										<!-- Image -->
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												@lang('admin.video_link')
											</label>
											<input required="required" type="text" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" name="video" value="{{$blog->video}}">
											<input type="hidden" name="type" value="video">	
										</div>
										<div class="m-portlet__foot m-portlet__foot--fit" >
											<div class="m-form__actions" style="margin-right: 450px;">
												<button type="Submit" class="btn btn-info">@lang('admin.savechanges')
												</button>
											</div>
										</div>
									</div>
								</form>
							
						@else
							<form class="m-form m-form--fit m-form--label-align-right" action="{{aurl('blog-update/'.$blog->id)}}" method="post" enctype="multipart/form-data">
								{{csrf_field()}}
								@method('put')						
								<div class="tab-content">
									@foreach(config('translatable.locales') as $locale)					<?php 
										$expanded='';
										if($locale=='ar' && app()->getLocale() == 'ar'){
													$expanded='active';
												}
										if($locale=='en' && app()->getLocale() == 'en'){
													$expanded='active';
												}
													?>	
									<div class="tab-pane {{$expanded}}" id="m_tabs_our_plus_4_{{$locale}}" role="tabpanel" aria-expanded="{{$expanded}}">
										<div class="m-portlet__body">				
											<!-- Section one -->
											<div class="form-group m-form__group">
												<label for="exampleInputEmail1">
													@lang('admin.title')
												</label>
												<input required="required" type="text" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" name="{{$locale}}[title]" value="{{$blog->{'title:'.$locale} }}">	
											</div>
											<!-- Description -->
											<div class="form-group m-form__group">
												<label for="exampleInputEmail1">
													@lang('admin.desc')
												</label>
												<textarea  required="required" type="text" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp"  name="{{$locale}}[desc]">{!!$blog->{'desc:'.$locale} !!}</textarea>
												<script>
								                        CKEDITOR.replace( '{{$locale}}[desc]' );
								                </script>
											</div>
											
											<!-- SEO Columns -->
											<h3 style="margin-top: 20px;">
													@lang('admin.seocolumns')
												</h3>
											<div class="form-group m-form__group">
												<label for="exampleInputEmail1">
													@lang('admin.title')
												</label>
												<div></div>
												<label class="custom-file">
													<input required="required" type="text"  class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp" name="{{$locale}}[seo_title]" value="{{$blog->{'seo_title:'.$locale} }}">
												</label>
											</div>
											<div class="form-group m-form__group">
												<label for="exampleInputEmail1">
													@lang('admin.desc')
												</label>
												<textarea  required="required" type="text" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp"  name="{{$locale}}[seo_desc]">{{$blog->{'seo_desc:'.$locale} }}</textarea>
											</div>
											<div class="form-group m-form__group">
												<label for="exampleInputEmail1">
													@lang('admin.key_words')
												</label>
												<textarea  required="required" type="text" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp"  name="{{$locale}}[key_words]">{{$blog->{'key_words:'.$locale} }}</textarea>
											</div>		
										</div>			
									</div>			
									@endforeach
									@if($blog->type=='projects')
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											@lang('admin.project_link')
										</label>
										<input required="required" type="text" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" name="link" value="{{$blog->link}}">
										<input type="hidden" name="type" value="projects">
									</div>
									@endif
									<!-- Image -->
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											@lang('admin.blogimage') 
										</label>
										<div></div>
										<label class="custom-file">
											<input  type="file" id="file2" class="custom-file-input image" name="image">
											<span class="custom-file-control"></span>
										</label>
									</div>
									<img src="{{asset('upload/'.$blog->image)}}" class="image-preview" style="width:100px;margin-right: 19px;">
									<div class="m-portlet__foot m-portlet__foot--fit" >
												<div class="m-form__actions" style="margin-right: 450px;">
													<button type="Submit" class="btn btn-info">@lang('admin.savechanges')
													</button>
												</div>
											</div>
								</div>
							</form>
						@endif
						</div>
					</div>
				</div>
				<!--end::Section-->
			</div>
			<!--end::Form-->
		</div>
	</div>
</div>
@include('admin2.modals.blog')
@endsection