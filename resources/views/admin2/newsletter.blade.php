@extends('admin2.index')
@section('content')
<div class="row">
	<div class="col-md-12" style="margin-top: 50px;">
		<div class="panel panel-default" >
			<div class="panel-heading">
				<h1 style="margin-bottom: 80px;">الايميلات المشتركه</h1>			
			</div>
			<div class="panel-body" style="">
				<table class="m-datatable__table" id="newsletter-table" style="display: block; width: 100%;">
					<thead class="m-datatable__head" style="direction: rtl;">
						<tr class="m-datatable__row" style=" ">
							<th>#</th>
							<th style="width: 500px;"><span style="">الإيميل </span></th>
							<th style="width: 500px;"><span style="">موعد الإشتراك</span></th>
							<th style="width: 500px;"><span style="">حذف</span></th>
						</tr>
					</thead>
					
				</table>
			</div>
		</div>		
	</div>
</div>

<div class="modal fade" id="email_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
							<div class="modal-dialog" role="document">
								<div class="modal-content" style="background-color: #ff5050;">
									<div class="modal-header">
										<h1 class="modal-title" id="exampleModalLabel" style="font-size: 30px;">
											تحذير
										</h1>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">
												ه
											</span>
										</button>
									</div>
									<div class="modal-body">
										<h2> هل انت متأكد من الحذف </h2>
										<form action="{{aurl('newsletter-delete')}}" method="post">
													{{ csrf_field() }}
													<input required="required" type="hidden" name="id" id="email-id">
													<input required="required" type="submit" class="btn btn-info"  style="width: 100px; margin-top: 10px;" value="{{trans('admin.savechanges')}}">
												</form>
									</div>
								</div>
							</div>
						</div>
@endsection