@include('admin2.includes.header')
@include('admin2.includes.nav')
@include('admin2.includes.aside')

	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<div class="m-content">
			@include('admin2.includes.messages')
			@yield('content')
		</div>
	</div>
					
						
</div>
			<!-- end:: Body -->

@include('admin2.includes.footer')
@include('admin2.includes.notify')