<div class="modal fade" id="cv_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
							<div class="modal-dialog" role="document">
								<div class="modal-content" style="background-color: #ff5050;">
									<div class="modal-header">
										<h1 class="modal-title" id="exampleModalLabel" style="font-size: 30px;">
											@lang('admin.confirm_delete')
										</h1>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">
												fcgvghvvgvvv
											</span>
										</button>
									</div>
									<div class="modal-body">
										<h2>@lang('admin.are_u_sure_delete')</h2>
										<form action="{{aurl('delete-cv')}}" method="post">
													{{ csrf_field() }}
													<input required="required" type="hidden" name="id" id="cv-id-delete">
													<input required="required" type="submit" class="btn btn-info"  style="width: 100px; margin-top: 10px;" value="@lang('admin.delete')">
												</form>
									</div>
								</div>
							</div>
						</div>