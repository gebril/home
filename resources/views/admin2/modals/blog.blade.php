<div class="modal fade" id="slider_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
							<div class="modal-dialog" role="document">
								<div class="modal-content" style="background-color: #ff5050;">
									<div class="modal-header">
										<h1 class="modal-title" id="exampleModalLabel" style="font-size: 30px;">
											@lang('admin.confirm_delete')
										</h1>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">
												ه
											</span>
										</button>
									</div>
									<div class="modal-body">
										<h2>@lang('admin.are_u_sure_delete')</h2>
										<form action="{{aurl('index/delete-slider')}}" method="post">
													{{ csrf_field() }}
													<input required="required" type="hidden" name="id" id="slider-id-delete">
													<input required="required" type="submit" class="btn btn-info"  style="width: 100px; margin-top: 10px;" value="@lang('admin.delete')">
												</form>
									</div>
								</div>
							</div>
						</div>


<div class="modal fade" id="cat_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
							<div class="modal-dialog" role="document">
								<div class="modal-content" style="background-color: #ff5050;">
									<div class="modal-header">
										<h1 class="modal-title" id="exampleModalLabel" style="font-size: 30px;">
											تحذير
										</h1>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">
												ه
											</span>
										</button>
									</div>
									<div class="modal-body">
										<h2> هل انت متأكد من الحذف </h2>
										<form action="{{aurl('delete-blogcategory')}}" method="post">
													{{ csrf_field() }}
													<input required="required" type="hidden" name="id" id="blogcat-id-form">
													<input required="required" type="submit" class="btn btn-info"  style="width: 100px; margin-top: 10px;" value="تأكيد الحذف ">
												</form>
									</div>
								</div>
							</div>
						</div>


<div class="modal fade" id="categories_updates" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					ضافة جديد لسكشن من نحن 
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
						×
					</span>
				</button>
			</div>
			<div class="modal-body">
				<form class="m-form m-form--fit m-form--label-align-right" action="{{aurl('update-blogcategory')}}" method="post" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="m-portlet">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<h3 class="m-portlet__head-text">
										Adjusted Pills
									</h3>
								</div>
							</div>
						</div>
						<div class="m-portlet__body">
							<ul class="nav nav-pills nav-fill" role="tablist">
								@foreach(config('translatable.locales') as $locale)
									<?php 
									$expanded='';
									if($locale=='ar' && app()->getLocale() == 'ar'){
												$expanded='active';
											}
									if($locale=='en' && app()->getLocale() == 'en'){
												$expanded='active';
											}
												?>
								<li class="nav-item">
									<a class="nav-link {{$expanded}}" data-toggle="tab" href="#m_tabs_cat_4_{{$locale}}" aria-expanded="true">
									@if($locale == 'ar')
										@lang('admin.arabic')
									@else
										@lang('admin.english')
									@endif
									</a>
								</li>
								@endforeach
							</ul>
									
								<div class="tab-content">
									@foreach(config('translatable.locales') as $locale)					<?php 
										$expanded='';
										if($locale=='ar' && app()->getLocale() == 'ar'){
													$expanded='active';
												}
										if($locale=='en' && app()->getLocale() == 'en'){
													$expanded='active';
												}
													?>	
									<div class="tab-pane {{$expanded}}" id="m_tabs_cat_4_{{$locale}}" role="tabpanel" aria-expanded="{{$expanded}}">
										<div class="m-portlet__body">				
											<!-- Section one -->
											<div class="form-group m-form__group">
												<label for="exampleInputEmail1">
													 {{$locale}}
												</label>
												<input required="required" type="text" class="form-control m-input" id="content-{{$locale}}" aria-describedby="emailHelp" placeholder="أدخل العنوان الخدمه " name="{{$locale}}[content]">	
											</div>			
										</div>			
									</div>
									@endforeach
									<div class="m-portlet__foot m-portlet__foot--fit" >
										<div class="m-form__actions" style="">
											<button type="Submit" class="btn m-btn--pill m-btn--air btn btn-info">         @lang('admin.savechanges')
											</button>
											<input required="required" type="hidden" name="id" id="edit-id-blogcat">
										</div>
									</div>
								</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- Blog Delete -->
<div class="modal fade" id="blog_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
							<div class="modal-dialog" role="document">
								<div class="modal-content" style="background-color: #ff5050;">
									<div class="modal-header">
										<h1 class="modal-title" id="exampleModalLabel" style="font-size: 30px;">
											تحذير
										</h1>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">
												ه
											</span>
										</button>
									</div>
									<div class="modal-body">
										<h2> هل انت متأكد من الحذف </h2>
										<form action="{{aurl('delete-blog')}}" method="post">
													{{ csrf_field() }}
													<input required="required" type="hidden" name="id" id="blog-id-form">
													<input required="required" type="submit" class="btn btn-info"  style="width: 100px; margin-top: 10px;" value="{{trans('admin.savechanges')}}">
												</form>
									</div>
								</div>
							</div>
						</div>




