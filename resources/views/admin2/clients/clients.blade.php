@extends('admin2.index')
@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
												@lang('admin.clients')
						</h3>
					</div>
				</div>
			</div>
			<div class="m-portlet__body">
				<!--begin::Section-->
				<div class="m-section">
					<div class="m-section__content">
							<table class="table table-striped m-table">
								<thead>
									<tr>
										<th>
											#
										</th>
										<th>
											@lang('admin.title')
										</th>
										<th>
											@lang('admin.client_image')
										</th>
										<th>
											@lang('admin.delete')
										</th>
										<th>
											@lang('admin.edit')
										</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=0;?>
									@foreach($clients as $client)
									<?php $i++; ?>
									<tr>
										<th scope="row" style="direction: rtl;">
											{{$i}}
										</th>
										<td style="direction: rtl;">
											{{$client->{'title:' . app()->getLocale()} }}
										</td>
										<td style="direction: rtl;">
											<img src="{{asset('upload/'.$client->image)}}"  style="width:70px; height: 70px; border-radius: 25px;">
										</td>
										
										<td style="direction: rtl;">
											<button type="button" class="btn m-btn--pill m-btn--air         btn-danger" id="delete-client" data-toggle="modal" data-target="#client_delete">
												<i class="fa fa-trash-o"></i>
												<input required="required" type="hidden" name="id" id="client-id" value="{{$client->id}}">
											</button>
										</td>
										<td style="direction: rtl;">
											<a class="btn m-btn--pill m-btn--air btn-info" href="{{aurl('edit-client/'.$client->id)}}"><i class="fa fa-edit" ></i> 
               								 </a>
										</td>
									</tr>
									@endforeach
							</tbody>
						</table>
						<hr>
						<div class="m-portlet__body">
							<ul class="nav nav-pills nav-fill" role="tablist">
								@foreach(config('translatable.locales') as $locale)
									<?php 
									$expanded='';
									if($locale=='ar' && app()->getLocale() == 'ar'){
												$expanded='active';
											}
									if($locale=='en' && app()->getLocale() == 'en'){
												$expanded='active';
											}
												?>
								<li class="nav-item">
									<a class="nav-link {{$expanded}}" data-toggle="tab" href="#m_tabs_our_plus_4_{{$locale}}" aria-expanded="true">
									@if($locale == 'ar')
										@lang('admin.arabic')
									@else
										@lang('admin.english')
									@endif
									</a>
								</li>
								@endforeach
							</ul>
							<form class="m-form m-form--fit m-form--label-align-right" action="{{aurl('client/add')}}" method="post" enctype="multipart/form-data">
								{{csrf_field()}}						
								<div class="tab-content">
									@foreach(config('translatable.locales') as $locale)					<?php 
										$expanded='';
										if($locale=='ar' && app()->getLocale() == 'ar'){
													$expanded='active';
												}
										if($locale=='en' && app()->getLocale() == 'en'){
													$expanded='active';
												}
													?>	
									<div class="tab-pane {{$expanded}}" id="m_tabs_our_plus_4_{{$locale}}" role="tabpanel" aria-expanded="{{$expanded}}">
										<div class="m-portlet__body">				
											<!-- Section one -->
											<div class="form-group m-form__group">
												<label for="exampleInputEmail1">
													@lang('admin.title')
												</label>
												<input required="required" type="text" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="أدخل العنوان الخدمه " name="{{$locale}}[title]">	
											</div>
											<!-- Description -->
											<div class="form-group m-form__group">
												<label for="exampleInputEmail1">
													@lang('admin.alt')
												</label>
												<textarea  required="required" type="text" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp"  name="{{$locale}}[alt]"></textarea>
											</div>		
										</div>			
									</div>			
									@endforeach
									<!-- Image -->
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											@lang('admin.client_image') 
										</label>
										<div></div>
										<label class="custom-file">
											<input required="required" type="file" id="file2" class="custom-file-input image" name="image">
											<span class="custom-file-control"></span>
										</label>
									</div>
									<img src="" class="image-preview" style="width:100px;margin-right: 19px;">
									<div class="m-portlet__foot m-portlet__foot--fit" >
												<div class="m-form__actions" style="margin-right: 450px;">
													<button type="Submit" class="btn btn-info">@lang('admin.savechanges')
													</button>
												</div>
											</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!--end::Section-->
			</div>
			<!--end::Form-->
		</div>
	</div>
</div>
@include('admin2.modals.clients')
@endsection