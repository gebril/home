<script type="text/javascript">
	$('#cvs-table').DataTable({
		processing: true,
        serverSide: true,
        "language": {
                  "search" : "{{trans('admin.search')}}",
                  "paginate": {
                      "previous": "{{trans('admin.previous')}}",
                      "next": "{{trans('admin.next')}}"
                  },
                  "info":           "{{trans('admin.show')}} _START_ {{trans('admin.to')}} _END_ {{trans('admin.from')}} _TOTAL_ {{trans('admin.from')}} {{trans('admin.rows')}}",
                  "lengthMenu":     "{{trans('admin.show')}} _MENU_ {{trans('admin.from')}} {{trans('admin.rows')}}",
                  "loadingRecords": "{{trans('admin.loadingRecords')}}",
                  "processing":     "{{trans('admin.processing')}}",
                  "zeroRecords":    "{{trans('admin.zeroRecords')}}",
                  "infoEmpty":      "{{trans('admin.show')}} 0 {{trans('admin.to')}} 0 {{trans('admin.from')}} 0  {{trans('admin.from')}} {{trans('admin.rows')}}",
                  "infoFiltered":   "(عرض من _MAX_ صف)",
              } ,
        ajax:"{{aurl('cvs-api')}}",
        columns: [
            {data:'id' , name:'id'},
            {data:'name' , name:'name'},
            {data:'job_title' , name:'job_title'},
            {data:'preview' , name:'preview' , orderable: false , searchable: false},
            {data:'download' , name:'download' , orderable: false , searchable: false},
            {data:'delete' , name:'delete' , orderable: false , searchable: false},
        ]

        });
</script>