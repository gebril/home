@extends('admin2.index')
@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
												@lang('admin.media_center')
						</h3>
					</div>
				</div>
			</div>
			<div class="m-portlet__body">
				<!--begin::Section-->
				<div class="m-section">
					<div class="m-section__content">
						<div class="m-portlet__body">
							<ul class="nav nav-pills nav-fill" role="tablist">
								@foreach(config('translatable.locales') as $locale)
									<?php 
									$expanded='';
									if($locale=='ar' && app()->getLocale() == 'ar'){
												$expanded='active';
											}
									if($locale=='en' && app()->getLocale() == 'en'){
												$expanded='active';
											}
												?>
								<li class="nav-item">
									<a class="nav-link {{$expanded}}" data-toggle="tab" href="#m_tabs_our_plus_4_{{$locale}}" aria-expanded="true">
									@if($locale == 'ar')
										@lang('admin.arabic')
									@else
										@lang('admin.english')
									@endif
									</a>
								</li>
								@endforeach
							</ul>
							<form class="m-form m-form--fit m-form--label-align-right" action="{{aurl('update-tv/'.$tv->id)}}" method="post" enctype="multipart/form-data">
								{{csrf_field()}}
								@method('put')						
								<div class="tab-content">
									@foreach(config('translatable.locales') as $locale)					<?php 
										$expanded='';
										if($locale=='ar' && app()->getLocale() == 'ar'){
													$expanded='active';
												}
										if($locale=='en' && app()->getLocale() == 'en'){
													$expanded='active';
												}
													?>	
									<div class="tab-pane {{$expanded}}" id="m_tabs_our_plus_4_{{$locale}}" role="tabpanel" aria-expanded="{{$expanded}}">
										<div class="m-portlet__body">				
											<!-- Section one -->
											<div class="form-group m-form__group">
												<label for="exampleInputEmail1">
													@lang('admin.video_title')
												</label>
												<input required="required" type="text" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="أدخل العنوان الخدمه " name="{{$locale}}[title]" value="{{$tv->{'title:'.$locale} }}">	
											</div>
										</div>			
									</div>			
									@endforeach
									<hr>
									<!-- Image -->
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											@lang('admin.video_link')
										</label>
										<input  type="text" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp"  name="video">	
									</div>
									<div class="m-portlet__foot m-portlet__foot--fit" >
										<div class="m-form__actions" style="margin-right: 450px;">
											<button type="Submit" class="btn btn-info">@lang('admin.savechanges')
											</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!--end::Section-->
			</div>
			<!--end::Form-->
		</div>
	</div>
</div>
@include('admin2.modals.tv')
@endsection