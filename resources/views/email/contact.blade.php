@component('mail::message')


Email : {{ $email }} <br>
Phone : {{ $phone }} 

Message : {{ $message }}


Thanks,<br>
{{ config('app.name') }}
@endcomponent
