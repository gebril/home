@extends('admin.index')
@section('content')

<form role="form" action="{{aurl('mainpage/update-section-1/' .  $section_1->id)}}" method="post" enctype="multipart/form-data">
	{{csrf_field()}}
	{{ method_field('put') }}
              <div class="box-body">
              	<!-- Web Logo -->
              	<div class="form-group">
                  <label for="exampleInputFile">اللوجو الاساسي للموقع </label>
                  <input type="file" id="exampleInputFile" class="image_logo" name="web_logo">               
                </div> 
                <img src="" class="image-preview-logo" style="width:100px;">
                <hr>
              	<!-- Title -->
                <div class="form-group">
                  <label for="exampleInputEmail1">عنوان الصفحه الرئيسيه </label>
                  <input type="text" class="form-control" id="exampleInputEmail1" placeholder="ادخل عنوانا مناسبا " value="{{$section_1->title}}" name="title" required="required">
                </div>
                  <label >الوصف </label>
                <div class="form-group">
                  {{-- Description --}}
                  
                    <textarea id="editor" name="desc" placeholder="Write Description" style="width: 500px; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required="required">{{$section_1->desc}}</textarea>
                  
                </div>
                <hr style="color: black">
                <div class="form-group">
                  <label>الفيديو </label>
                  <input type="text" class="form-control" id="exampleInputPassword1" placeholder="اضف لينك فيديو اليوتيوب " name="video" value="{{$section_1->video}}">
                </div>
                <hr>
                <!-- clients -->
                <div class="form-group">
                  <label>عملاء راضون  </label>
                  <input type="number" class="form-control"  placeholder="اضف عدد العملاء الراضون" name="clients" value="{{$section_1->clients}}">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">لوجو العملاء</label>
                  <input type="file" id="exampleInputFile" class="image" name="cliend_logo">               
                </div> 
                <img src="" class="image-preview" style="width:100px;">
                <hr>
                <!-- years -->
                <div class="form-group">
                  <label>سنوات الخبره </label>
                  <input type="number" class="form-control"  placeholder="اضف عدد  سنوات الخبره" name="years" value="{{$section_1->years}}">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile" >لوجو سنوات الخبره </label>
                  <input type="file" id="exampleInputFile"class="image_a" name="year_logo">               
                </div>  
                <img src="" class="image-preview-a" style="width:100px;">
                <hr>
                <!-- Disscution -->   
                <div class="form-group">
                  <label>استشارات مجانيه</label>
                  <input type="number" class="form-control"  placeholder="اضف عدد  الاستشارات القانونيه" name="disscutions" value="{{$section_1->disscutions}}">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">لوجو الاستشارات المجانيه </label>
                  <input type="file" id="exampleInputFile" class="image_b" name="disscution_logo">               
                </div> 
                <img src="" class="image-preview-b" style="width:100px;">
                <hr>
                <!-- projects -->
                <div class="form-group">
                  <label>المشاريع</label>
                  <input type="number" class="form-control"  placeholder="اضف عدد  المشاريع" name="projects" value="{{$section_1->projects}}">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">لوجو  المشاريع</label>
                  <input type="file" id="exampleInputFile" class="image_c" name="project_logo">               
                </div>   
                <img src="" class="image-preview-c" style="width:100px;">      
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">تحديث البيانات</button>
              </div>
            </form>


@endsection