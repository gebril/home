
<div class="modal fade" id="modal-default" style="display: none;">
     <div class="modal-dialog">
         <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">تعديل بيانات المشرف</h4>
              </div>
              <div class="modal-body">
	                <div class="box box-info">
	            <!-- /.box-header -->
		            <!-- form start -->
		            <div class="form-horizontal">

		              <div class="box-body">
		                <div class="form-group">
		                  <label for="name" class="col-sm-2 control-label">الاسم</label>

		                  <div class="col-sm-10">
		                    <input type="text" class="form-control" id="name" placeholder="الاسم" value="{{auth()->user()->name}}">
		                  </div>
		                </div>
		                <div class="form-group">
		                  <label for="email" class="col-sm-2 control-label">البريد الالكتروني</label>

		                  <div class="col-sm-10">
		                    <input type="email" class="form-control" id="email" placeholder="البريد الالكتروني"  value="{{auth()->user()->email}}">
		                    <input type="hidden" class="form-control" id="id" placeholder="البريد الالكتروني"  value="{{auth()->user()->id}}">
		                  </div>
		                </div>
		                <div class="form-group">
		                  <label for="password" class="col-sm-2 control-label">كلمة السر </label>

		                  <div class="col-sm-10">
		                    <input type="password" class="form-control" id="password" placeholder="كلمة السر" >
		                  </div>
		                </div>		                
		              </div>
		              <!-- /.box-body -->		      
		            </div>
		          </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">اغلاق</button>
                <button type="button" class="btn btn-primary" id="savechanges">حفظ التغييرات</button>
              </div>
            </div>
            {{csrf_field()}}
            <!-- /.modal-content -->
  </div>
          <!-- /.modal-dialog -->
 </div>

