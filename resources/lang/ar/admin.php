<?php



return [
	'esnad'=>'اسناد',
	'Sign In To Admin'=>'تسجيل الدخول الي لوحة التحكم',
	'Remember me'=>'تذكرني',
	'index'=>'الصفحه الرئيسيه',
	'slider'=>'الإسلايدر',
	'create slider'=>'اضافة سلايدر',
	'sub_title'=>'عنوان فرعي',
	'desc'=>'الوصف',
	'slider_added'=>'تم اضافة اسلايدر بنجاح',
	'ok'=>'شكرا',
	'success'=>'تمت العمليه بنجاح',
	'Delete'=>'حذف',
	'Edit'=>'تعديل',
	//datatable
	'search'=>'بحث',
	'previous'=>'السابق',
	'next'=>'التالي',
	'show'=>'عرض',
	'to'=>'الي',
	'from'=>'من',
	'rows'=>'الصفوف',
	'loadingRecords'=>'جاري التحميل...',
	'processing'=>'جاري التحميل...',
	'zeroRecords'=>'لا يوجد نتائج',
	//end datatable
	'Add New'=>'اضافة جديد',
	'images'=>'صور',
	'notfound_error'=>'لا يوجد ',
	'error'=>'خطأ',
	'confirm_delete'=>'تفعيل الحذف',
	'are_u_sure_delete'=>'هل أنت متأكد من الحذف',
	'item_deleted'=>'تم حذف العنصر بنجاح',
	'item_updated'=>'تم تحديث العنصر بنجاح ',
	'about_esnad'=>'عن إسناد',
	'categories_services'=>'اقسام الخدمات',
	'categories_about_esnad'=>'اقسام عن إسناد',
	'categories_about_esnad'=>'اقسام عن إسناد',
	'edit_client'=>'تعديل بيانات العميل',
	'alt'=>'تدليل لصوره',
	'client_image'=>'صورة العميل',
	'media_center'=>'خدمات فيديو',
	'news'=>'الأخبار',
	'jobs'=>'الموارد البشريه',
	'video_link'=>'لينك الفيديو',
	'video'=>'يديو',
	'video_title'=>'عنوان الفيديو',
	'video_added'=>'تم إضافة الفيديو بنجاح',
	'video_updated'=>'تم تحديث الفيديو بنجاح',
	'tv_meetings'=>'خدمات فيديوهات',
	'edit_admin'=>'تعديل بيانات المشرف',
	'create_slider'=>'إضافة سلايدر جديد ',
	'edit_slider'=>'تعديل سلايدر  ',
	'edit_video'=>'تعديل الفيديو  ',
	'about_us'=>'من نحن',
	'about_us_image'=>'صورة من نحن',
	'about_us_updated'=>'تم تجديث صفحة من نحن بنجاح',
	'base_data'=>'البيانات الأساسيه',
	'address'=>'العنوان',
	'email'=>'الإيميل الأساسي للموقع',
	'services'=>'الخدمات',
	'number1'=>'الرقم الأول',
	'number2'=>'الرقم الثاني',
	'facebook'=>'الفيسبوك',
	'twitter'=>'تويتر',
	'instgram'=>'إنستجرام',
	'youtube'=>'اليوتيوب',
	'data_updated'=>'تم تحديث البيانات بنجاح',
	'clients_seo'=>'أرشفة صفحة العملاء',
	'service_seo'=>'أرشفة صفحة الخدمات',
	'contact_us_seo'=>'أرشفة صفحة إتصل بنا ',
	'contact_us'=>'إتصل بنا',
	'news_seo'=>'أرشفة صفحة الأخبار  ',
	'cv_jobs'=>'طلبات التوظيف',
	'preview'=>'معاينة',
	'job_title'=>'العنوان الوظيفي',
	'name'=>'الإسم',
	'download'=>'تحميل',
	'vision1'=>'الرؤيه',
	'message'=>'الرساله',
	'founder_word'=>'كلمةالمدير التنفيذي',
	'url'=>'لينك الموقع',
	///////////////////
	'mainpage'=>'الصفحه الرئيسيه',
	'section_1'=>'السكشن الأول',
	'section_2'=>'السكشن الثاني',
	'section2'=>'السكشن الثاني',
	'service'=>' الخدمات',
	'mobile_app'=>'تطبيقات الهاتف الجوال',
	'ecommerce'=>'التجاره الالكترونيه',
	'hosts'=>'الاستضافات',
	'lang'=>'اللغه الانجليزيه',
	'arabic'=>'اللغه العربيه',
	'english'=>'اللغه الانجليزيه',
	'site_logo'=>'لوجو الموقع',
	'site_title'=>'عنوان الموقع ',
	'section_title'=>'عنوان السكشن',
	'section1_desc'=>'وصف السكشن الاول',
	'desc'=>'الوصف',
	'video'=>'الفيديو',
	'clients'=>'العملاء',
	'clients_logo'=>'لوجو العملاء',
	'years_exp'=>'سنوات الخبره',
	'years_exp_logo'=>'لوجو سنوات الخبره ',
	'consultation'=>'الإستشارات المجانيه',
	'consultation_logo'=>'لوجو الإستشارات المجانيه',
	'projects'=>'المشاريع',
	'projects_logo'=>'لوجو المشاريع',
	'manager_photo'=>'صورة المدير التنفيذي',
	'manager_desc'=>'كلمة المدير التنفيذي',
	'manager_name'=>'اسم المدير التنفيذي',
	'savechanges'=>'حفظ التغييرات',
	'vision'=>'فجن',
	'add'=>'اضافة جديد',
	'title'=>'عنوان',
	'logo'=>'اللوجو',
	'delete'=>'حذف',
	'edit'=>'تعديل',
	'addto'=>'اضافه الي العنصر',
	'webdesign'=>'تصميم المواقع ',
	'government'=>'المواقع الحكوميه',
	'webperson'=>'مواقع إلكترونيه للأشخاص',
	'projects'=>'المشاريع',
	'categories'=>'الأقسام',
	'projectlink'=>'لينك المشروع',
	'projectimage'=>'صورة المشروع',
	'projectcat'=>'اقسام المشاريع',
	'deletesure'=>'هل أنت متأكد من الحذف؟',
	'danger'=>'تحذير',
	'delete'=>'حذف',
	'blog'=>'المدونه',
	'blogs'=>'المدونات',
	'seocolumns'=>'الأرشفه',
	'content'=>'المحتوي',
	'key_words'=>'كلمات مفتاحيه',
	'blogimage'=>'صورة المدونه',
	'in'=>' ب',
	'dashboard'=>'لوحة التحكم',
	'logout'=>'تسجيل الخروج',
	'companysites'=>'مواقع الكترونيه للشركات ',
	'ecommercesites'=>'مواقع التجاره الالكترونيه',
	'serviceimage'=>'صورة الخدمه ',
	'ecommercefeature'=>'مميزات المتاجر الإلكترونيه',
	'section2title'=>'Section(2) Title',
	'project_link'=>'لينك المشروع',
	'select_blog'=>'إختر الخدمة',
	'add_projects'=>'إضافة مشاريع',
	'videos'=>'فيديوهات',
	'email2'=>'الايميل',
	'password'=>'كلمة السر',
	'update_profile'=>'تحديث البيانات الشخصيه',
];



?>