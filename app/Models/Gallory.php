<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gallory extends Model
{
    protected $fillable=['locale','image','slider_id'];

    public function slider()
    {
    	return $this->belongsTo(Slider::class);
    }
}
