<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SeoTranslation extends Model
{
    protected $fillable=['seo_title','seo_desc','key_words'];
}
