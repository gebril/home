<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seo extends Model
{
    use \Dimsav\Translatable\Translatable;
	public $translatedAttributes = ['seo_title','seo_desc','key_words'];
	protected $fillable=['type'];
}
