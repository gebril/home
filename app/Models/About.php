<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
     use \Dimsav\Translatable\Translatable;
    protected $fillable=['image'];
    public $translatedAttributes = ['title','desc','seo_title','seo_desc','key_words'];
}
