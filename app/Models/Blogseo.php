<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blogseo extends Model
{
     use \Dimsav\Translatable\Translatable;
    public $translatedAttributes = ['seo_title','seo_desc','key_words'];
}
