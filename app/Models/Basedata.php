<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Basedata extends Model
{
    use \Dimsav\Translatable\Translatable;
    protected $fillable=['email','number1','number2','facebook','twitter','instgram','youtube','url'];
    public $translatedAttributes = ['address','vision','message','founder','seo_title','seo_desc','key_words','logo'];
}
