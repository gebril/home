<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
	use \Dimsav\Translatable\Translatable;
	public $translatedAttributes = ['title','sub_title','desc','seo_title','seo_desc','key_words'];
	public function gallories()
    {
    	return $this->hasMany(Gallory::class);
    }

}
