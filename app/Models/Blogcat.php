<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blogcat extends Model
{
    use \Dimsav\Translatable\Translatable;
    public $translatedAttributes = ['content'];
}
