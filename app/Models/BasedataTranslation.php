<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BasedataTranslation extends Model
{
    protected $fillable=['address','vision','message','founder','seo_title','seo_desc','key_words'];
}
