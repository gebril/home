<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['title','desc','seo_title','seo_desc','key_words'];

    public function blog()
    {
    	return $this->belongsTo(Blog::class);
    }
}
