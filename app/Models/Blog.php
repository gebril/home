<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use \Dimsav\Translatable\Translatable;
    protected $fillable=['image','type','parent_id','link','video'];
    public $translatedAttributes = ['title','desc','seo_title','seo_desc','key_words'];

}
