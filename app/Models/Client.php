<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use \Dimsav\Translatable\Translatable;
    protected $fillable=['image'];
    public $translatedAttributes = ['title','alt'];
}
