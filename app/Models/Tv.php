<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tv extends Model
{
    use \Dimsav\Translatable\Translatable;
	public $translatedAttributes = ['title'];
	protected $fillable=['video'];
}
