<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TvTranslation extends Model
{
    protected $fillable=['title'];
}
