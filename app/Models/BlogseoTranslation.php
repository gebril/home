<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogseoTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['seo_title','seo_desc','key_words'];
}
