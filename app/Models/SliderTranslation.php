<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SliderTranslation extends Model
{
    protected $fillable=['title','sub_title','desc','seo_title','seo_desc','key_words','slider_id'];
    public function slider()
    {
    	return $this->belongsTo(Slider::class);
    }
}
