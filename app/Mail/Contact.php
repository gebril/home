<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Contact extends Mailable
{
    use Queueable, SerializesModels;
    public $email;
    public $phone;
    public $message;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mail,$phone_number,$msg)
    {
        $this->email=$mail;
        $this->phone=$phone_number;
        $this->message=$msg;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('email.contact')->with([
            'email'=>$this->email,
            'phone'=>$this->phone,
            'message'=>$this->message
        ]);
    }
}
