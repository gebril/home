<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use yajra\Datatables\Datatables;
use App\Models\Basedata;
use App\Models\Job;
class BaseController extends Controller
{
    public function index()
    {
    	$about=Basedata::first();
    	return view('admin2.index.base_data',compact('about'));
    }
    public function update(Request $request)
    {
    	$data=$request->except(['_token','logo']);
    	if ($request->hasFile('ar[logo]')) {
    		$data['ar[logo]']=image_upload($request->ar['logo']);
    	}
        if ($request->hasFile('en[logo]')) {
            $data['en[logo]']=image_upload($request->en['logo']);
        }
        
    	$about=Basedata::first();
    	$about->update($data);
    	session()->flash('success',trans('admin.data_updated'));
    	return back();
	}
	
	public function cvs_api()
	{
		$jobs=Job::all();
		return Datatables::of($jobs)
		->addColumn('preview',function($row){
    		return '
                <a class="btn btn-success" target="_blank" href=" ' .aurl("cv-preview/".$row->id).'" style="margin-right:35px;"><i class="fa fa-eye" ></i> 
                </a>
                    
                ';
    	})->addColumn('download',function($row){
    		return '
                <a class="btn m-btn m-btn--gradient-from-primary m-btn--gradient-to-info" target="_blank" href=" ' .url("upload/cvs/".$row->cv).'" style="margin-right:35px;"><i class="fa fa-download" ></i> 
                </a>
                    
                ';
    	})->addColumn('delete',function($row){
    		return '<a class="btn btn-danger" data-toggle="modal" data-target="#cv_delete" id="delete-cv" style="margin-right:38px;"><i class="fa fa-trash-o"></i> <input type="hidden"  id="mycv_id" value="' . $row->id . '"> </a>
                ';
    	})->rawColumns(['preview'=>'preview','download'=>'download','delete' => 'delete'])
    	->make(true);
	}
	public function cvs()
	{
		return view('admin2.cvs.cvs');
	}
	public function cv_preview($id)
	{
		$cv=Job::findOrFail($id);
		return response()->file('upload/cvs/'.$cv->cv);
	}
	public function cv_delete(Request $request)
	{
		$job=Job::findOrFail($request->id);
		$job->delete();
		// Job::destroy($request->id);
    	session()->flash('success' , 'Deleted Successfully');
    	return back();
	}
}
