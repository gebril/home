<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Newsletter;
use yajra\Datatables\Datatables;
class AdminController extends Controller
{
    public function index()
    {
        $title=trans('admin.esnad');
        return view('admin2.index',compact('title'));
    }
    public function edit()
    {
        $title=trans('admin.edit_admin');
        return view('admin2.editprofile');
    }

    public function update(Request $request)
    {
    	$admin= User::find(auth()->user()->id);
    	$admin->name=$request->name;
    	$admin->email=$request->email;
    	$admin->password=bcrypt($request->password);
    	$admin->save();
        session()->flash('success' , 'Profile Updated Succesfully');
    	return back();
    }
    

    public function newsletter()
    {
        return view('admin2.newsletter');
    }

    public function api_newsletter()
    {
        $emails=Newsletter::all();
        return Datatables::of($emails)
           ->addColumn('delete' , function($row){
            return '
                <a class="btn btn-danger" data-toggle="modal" data-target="#email_delete" id="delete-email" style="margin-bottom:10px;"><i class="fa fa-trash-o"></i>
                <input type="hidden" name="" id="emailId" value="' . $row->id . '">
                </a>';
            })->rawColumns(['delete' => 'delete'])

        ->make(true);
    }
    public function delete_email(Request $request)
    {
        Newsletter::destroy($request->id);
        session()->flash('success' , 'Deleted Succesfully');
        return back();
    }
}
