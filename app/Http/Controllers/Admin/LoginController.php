<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
class LoginController extends Controller
{
    public function login_page()
    {
    	return view('admin2.login');
    }

    public function login()
    {
    	$remember=request('remember')==1 ? true : false;
    	if(auth()->attempt(['email'=>request('email'), 'password'=>request('password')],$remember)){
    		return redirect('admin-panel/admin');
    	}else{
    		session()->flash('errors' , 'هناك خطأ في الايميل او الرقم السري');
    		return redirect('admin-panel/login');
    	}
    }

    public function logout()
    {
    	auth()->logout();
    	return redirect('admin-panel/login');
    }
}
