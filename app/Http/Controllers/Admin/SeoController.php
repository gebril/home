<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Seo;
class SeoController extends Controller
{
    public function update(Request $request,$id)
    {
    	$data=$request->except(['_token','_method']);
    	 $seo=Seo::findOrFail($id);
    	$seo->update($data);
    	session()->flash('success',trans('admin.seo_updated'));
    	return back();
    }
    public function service_seo()
    {
        $seo=Seo::where('type','service')->first();
        $title=trans('admin.service_seo');
        $type='service';
        return view('admin2.clients.seo',compact('seo','title','type'));
    }
    public function job_seo()
    {
        $seo=Seo::where('type','jobs')->first();
        $title=trans('admin.job_seo');
        $type='jobs';
        return view('admin2.clients.seo',compact('seo','title','type'));
    }
    public function tv_seo()
    {
        $seo=Seo::where('type','tv')->first();
        $title=trans('admin.tv_seo');
        $type='tv';
        return view('admin2.clients.seo',compact('seo','title','type'));
    }
    public function contactus_seo()
    {
        $seo=Seo::where('type','contactus')->first();
        $title=trans('admin.contact_us_seo');
        $type='contactus';
        return view('admin2.clients.seo',compact('seo','title','type'));
    }
    public function news_seo()
    {
        $seo=Seo::where('type','news')->first();
        $title=trans('admin.news_seo');
        $type='news';
        return view('admin2.clients.seo',compact('seo','title','type'));
    }
}
