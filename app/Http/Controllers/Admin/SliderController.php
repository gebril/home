<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use yajra\Datatables\Datatables;
use App\Models\Slider;
use App\Models\Gallory;
class SliderController extends Controller
{
	public function index()
	{
        $title=trans('admin.slider');
		return view('admin2.index.slider.index',compact('title'));
	}
    public function create()
    {
        $title=trans('admin.create_slider');
    	return view('admin2.index.slider.create',compact('title'));
    }
    public function store(Request $request)
    {
    	$request->validate([
    		'title.*'=>'required|min:3',
    		'sub_title.*'=>'required|min:3',
    		'desc.*'=>'required|min:5',
    		'image.*'=>v_image(),
    	]);
    	$data=$request->except(['image_ar','image_en','_token']);
    	$slider=Slider::create($data);
    	if ($request->has('image_ar')) {
    		foreach ($request->image_ar as $image) {
    			$img= image_upload($image);
    			Gallory::create(['image'=>$img,'locale'=>'ar','slider_id'=>$slider->id]);
    		}
		}
		if ($request->has('image_en')) {
    		foreach ($request->image_en as $image) {
    			$img= image_upload($image);
    			Gallory::create(['image'=>$img,'locale'=>'en','slider_id'=>$slider->id]);
    		}
    	}
    	session()->flash('success',trans('admin.slider_added'));
    	return back();
    }

    public function slider_api()
    {
    	$sliders=Slider::all();
    	return Datatables::of($sliders)
    	->addColumn('title',function($row){
    		return $row->{'title:'.app()->getLocale()};
    	})->addColumn('sub_title',function($row){
    		return $row->{'sub_title:'.app()->getLocale()};
    	})->addColumn('delete',function($row){
    		return '<a class="btn btn-danger" data-toggle="modal" data-target="#slider_delete" id="delete-slider" style="margin-right:38px;"><i class="fa fa-trash-o"></i> <input type="hidden"  id="myslider_id" value="' . $row->id . '"> </a>
                ';
    	})->addColumn('edit',function($row){
    		return '
                <a class="btn m-btn m-btn--gradient-from-primary m-btn--gradient-to-info" href=" ' .aurl("index/edit-slider/".$row->id).'" style="margin-right:35px;"><i class="fa fa-edit" ></i> 
                </a>
                    
                ';
    	})->rawColumns(['title'=>'title','sub_title'=>'sub_title','delete' => 'delete' , 'edit'=>'edit'])
    	->make(true);
    }
    public function edit($id)
    {
    	if($data['slider']=Slider::findOrFail($id)){	
    	$data['title']='Edit Slider';
    	$data['images']=$data['slider']->gallories;
        $data['title']=trans('admin.edit_slider');
    	return view('admin2.index.slider.edit')->with($data);
    	}else{
    		session()->flash('error',trans('admin.notfound_error'));
    		return back();
    	}
    }
    public function update(Request $request,$id)
    {
    	$request->validate([
    		'title.*'=>'required|min:3',
    		'sub_title.*'=>'required|min:3',
    		'desc.*'=>'required|min:5',
    		'image.*'=>v_image(),
    	]);
    	$data=$request->except(['image_ar','image_en','_token','_method']);
    	$slider=Slider::findOrFail($id);
        foreach($slider->gallories as $gal){
            $gal->delete();
        }
    	$slider->update($data);
    	if ($request->has('image_ar')) {
    		foreach ($request->image_ar as $image) {
    			$img= image_upload($image);
    			$slider->gallories()->create(['image'=>$img,'locale'=>'ar']);
    		}
		}
		if ($request->has('image_en')) {
    		foreach ($request->image_en as $image) {
    			$img= image_upload($image);
    			$slider->gallories()->create(['image'=>$img,'locale'=>'en']);
    		}
    	}
    	session()->flash('success',trans('admin.item_updated'));
    	return redirect(aurl('index/sliders'));
    }
    public function delete(Request $request)
    {
    	Slider::destroy($request->id);
    	session()->flash('success',trans('admin.item_deleted'));
    	return back();
    }
}
