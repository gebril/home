<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Blogcat;
use App\Models\Blog;
use App\Models\Blogseo;
class BlogController extends Controller
{
    public function categories()
    {
        $cats=Blogcat::all();
        return view('admin2.blogs.categories',compact('cats'));
    }
    public function add_category(Request $request)
    {
        $data=$request->except('_token');
        Blogcat::create($data);
        session()->flash('success' , 'Created Successfully');
        return back();
    }

    public function delete_category(Request $request)
    {
        Blogcat::destroy($request->id);
        session()->flash('success' , 'Deleted Successfully');
        return back();
    }
    public function update_category(Request $request)
    {
        $blogject=Blogcat::findOrFail($request->id);
        $data=$request->except('_token','id');
        $blogject->update($data);
        session()->flash('success' , 'Updated Successfully');
        return back();
    }

    public function blogs($type)
    {
        if($type=='about_esnad'||$type=='service'||$type=='news'||$type=='projects'){
            $blogs=Blog::where('type',$type)->get();
            $all_blogs=Blog::where('type','service')->get();
            $title=trans('admin.'.$type);
            return view('admin2.blogs.blogs',compact('blogs','type','title','type','all_blogs'));
        }else{
            session()->flash('error',trans('admin.notfound_error'));
            return back();
        }
    }
    public function blog_edit($id)
    {
        $blog=Blog::findOrFail($id);
        $blogcats=Blogcat::all();
        $all_blogs=Blog::where('type','service')->get();
        $title=trans('admin.edit');
        return view('admin2.blogs.edit',compact('blog','blogcats','title','all_blogs'));
    }

    public function add_blog(Request $request)
    {
        $rules=[
            'ar.title'=>'unique:blog_translations,title',
            'en.title'=>'unique:blog_translations,title'
        ];
        $request->validate($rules);
        if($request->type=='about_esnad'||$request->type=='service'||$request->type=='news'||$request->type=='projects'){
            $data=$request->except('_token','image');
            if ($request->hasFile('image')) {
                $data['image']= image_upload($request->image);
            }
            if ($request->video) {
                $num=strcspn($request->video,"=");
                $num=$num+1;
                $code=substr($request->video,$num);
                $num2=strcspn($code,"&");
                $code2=substr_replace($code,'',$num2);
                $num3=strcspn($code2,"=");
                $code3=substr_replace($code2,'',$num3);
                $data['video']=$code3;
            }
            Blog::create($data);
            session()->flash('success' , 'Created Successfully');
            return back();
        }else{
            session()->flash('error',trans('admin.notfound_error'));
            return back();
        }
    }
    public function blog_update(Request $request,$id)
    {
        $data=$request->except('_token','image');
        if ($request->hasFile('image')) {
        $data['image']= image_upload($request->image);
        }
        $blog=Blog::findOrFail($id);
        $blog->update($data);
        session()->flash('success' , 'Updated Successfully');
        return back();
    }

    public function delete_blog(Request $request)
    {
        Blog::destroy($request->id);
        session()->flash('success' , 'Deleted Successfully');
        return back();
    }
    public function blog_add_projects(Request $request,$type,$id)
    {
        $blog=Blog::findOrFail($id);
        if($blog->type!='service'){
            session()->flash('error',trans('admin.notfound_error'));
            return back();
        }

        
        $blogs=Blog::where('parent_id',$id)->where('type',$type)->get();
        //$all_blogs=Blog::where('type','service')->get();
        $title=trans('admin.projects');
        
        return view('admin2.blogs.blogs',compact('blogs','title','type','id'));
        
    }
    public function blog_add_projects_post(Request $request,$id)
    {
        if($request->type=='projects'||$request->type=='video'){
            $data=$request->except('_token','image');
            if ($request->hasFile('image')) {
            $data['image']= image_upload($request->image);
            }
            $data['parent_id']=$id;
            $data['type']=$request->type;
            Blog::create($data);
            session()->flash('success' , 'Created Successfully');
            return back();
        }else{
            session()->flash('error',trans('admin.notfound_error'));
            return back();
        }
    }
    public function blogseo(Request $request)
    {
        $seo=Blogseo::first();
        return view('admin2.blogs.seo',compact('seo'));
    }
    public function blogseo_update(Request $request)
    {
        $data=$request->except('_token','/ar/admin-panel/update-blogseo','/en/admin-panel/update-blogseo');
        $seo=Blogseo::first();
        $seo->update($data);
        session()->flash('success' , 'Updated Successfully');
        return back();
    }
}
