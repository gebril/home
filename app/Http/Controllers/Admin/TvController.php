<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tv;
class TvController extends Controller
{
    public function index()
    {
    	$tvs=Tv::all();
    	$title=trans('admin.tv_meetings');
    	return view('admin2.media.create',compact('tvs','title'));
    }
    public function store(Request $request)
    {
    	$data=$request->except('_token','video');
    	if($request->video){
            $num=strcspn($request->video,"=");
            $num=$num+1;
            $code=substr($request->video,$num);
            $num2=strcspn($code,"&");
            $code2=substr_replace($code,'',$num2);
            $num3=strcspn($code2,"=");
            $code3=substr_replace($code2,'',$num3);
            $data['video']=$code3;
        }
        Tv::create($data);
        session()->flash('success',trans('admin.video_added'));
    	return back();
    }
    public function update(Request $request,$id)
    {
    	$tv=Tv::findOrFail($id);
    	$data=$request->except('_token','_method','video');
    	if($request->video!=null){
            $num=strcspn($request->video,"=");
            $num=$num+1;
            $code=substr($request->video,$num);
            $num2=strcspn($code,"&");
            $code2=substr_replace($code,'',$num2);
            $num3=strcspn($code2,"=");
            $code3=substr_replace($code2,'',$num3);
            $data['video']=$code3;
        }
        $tv->update($data);
        session()->flash('success',trans('admin.video_updated'));
    	return back();
    }
    public function edit($id)
    {
    	$tv=Tv::findOrFail($id);
    	$title=trans('admin.edit_video');
    	return view('admin2.media.edit',compact('tv','title'));
    }
    public function delete(Request $request)
    {
    	Tv::destroy($request->id);
    	session()->flash('success',trans('admin.item_deleted'));
    	return back();
    }
}
