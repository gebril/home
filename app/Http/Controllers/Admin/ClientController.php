<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\Seo;
class ClientController extends Controller
{ 
    public function index()
    {
        $clients=Client::all();
        $title=trans('admin.clients');
            return view('admin2.clients.clients',compact('clients','title'));
    }
    public function store(Request $request)
    {
    	$data=$request->except('_token','image');
        if ($request->hasFile('image')) {
            $data['image']= image_upload($request->image);
            }
            
         Client::create($data);
         session()->flash('success' , 'Created Successfully');
          return back();
    }
    public function edit($id)
    {
        $client=Client::findOrFail($id);
        $title=trans('admin.edit_client');
            return view('admin2.clients.edit',compact('client','title'));
    }
    public function delete(Request $request)
    {
    	Client::destroy($request->id);
    	session()->flash('success' , 'Deleted Successfully');
    	return back();
    }
    public function update(Request $request,$id)
    {
    	$data=$request->except('_token','_method','image');
    	$client=Client::findOrFail($id);
        if ($request->hasFile('image')) {
            $data['image']= image_upload($request->image);
            }
            
         $client->update($data);
         session()->flash('success' , 'Updated Successfully');
          return redirect(aurl('clients'));
    }
    public function seo()
    {
        $seo=Seo::where('type','client')->first();
        $title=trans('admin.clients_seo');
        $type='client';
        return view('admin2.clients.seo',compact('seo','title','type'));
    }
}
