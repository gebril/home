<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\About;
class AboutController extends Controller
{
    public function index()
    {
        $title=trans('admin.about_us');
        $about=About::first();
        return view('admin2.index.about_us.about_us',compact('title','about'));
    }
    public function update(Request $request)
    {
    	$data=$request->except(['_token','image']);
    	if ($request->hasFile('image')) {
    		$data['image']=image_upload($request->image);
    	}
    	$about=About::first();
        $about->update($data);
        session()->flash('success',trans('admin.about_us_updated'));
        return back();
    }
}
