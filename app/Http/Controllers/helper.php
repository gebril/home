<?php


if (!function_exists('section_1')) {
	function section_1() {
		return \App\Models\section_1::orderBy('id', 'desc')->first();
	}
}

if(!function_exists('aurl')){
	function aurl($url=null){
		return url( app()->getLocale() . '/admin-panel/' . $url);
	}
}
if(!function_exists('murl')){
	function murl($url=null){
		return url( app()->getLocale() . '/' . $url);
	}
}
if(!function_exists('furl')){
	function furl(){
		return url( 'front/'.app()->getLocale() );
	}
}

if(!function_exists('admin')){
	function admin(){
		return auth()->user();
	}
}
if(!function_exists('active_menu')){
	function active_menu($link){
		if (preg_match('/' . $link . '/i', Request::segment(2))) {
			return ['active-menu' , 'display:block'];
		}else{
			return ['' , ''];
		}
	}
}
if(!function_exists('v_image')){
	function v_image($ext=null){
		if ($ext===null) {
			return 'image|mimes:jpg,jpeg,png,gif';
		}else{
			return 'image|mimes:' . $ext;
 		}
	}
}
if(!function_exists('v_file')){
	function v_file($ext=null){
		if ($ext===null) {
			return 'required|mimes:pdf,docx';
		}else{
			return 'mimes:' . $ext;
 		}
	}
}
if (!function_exists('image_upload')){
	function image_upload($file,$image_name=null)
	{
		$image_name=md5 (microtime()) . '.' . $file->getClientOriginalExtension();
		 $file->move(public_path('upload'),$image_name);
		 return $image_name;
	}
}
if (!function_exists('cv_upload')){
	function cv_upload($file,$file_name=null)
	{
		$file_name=md5 (microtime()) . '.' . $file->getClientOriginalExtension();
		 $file->move(public_path('upload/cvs/'),$file_name);
		 return $file_name;
	}
}
if (!function_exists('video_link')){
    function video_link($video)
    {
        $num=strcspn($video,"=");
        $num=$num+1;
        $code=substr($video,$num);
        $num2=strcspn($code,"&");
        $code2=substr_replace($code,'',$num2);
        $num3=strcspn($code2,"=");
        $code3=substr_replace($code2,'',$num3);
        return $code3;
    }
}


?>