<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Basedata;
use App\Models\Blog;
use App\Models\BlogTranslation;
use App\Models\Tv;
use App\Models\Seo;
use App\Models\Client;
use App\Models\Slider;
use App\Models\SliderTranslation;
use App\Models\Job;
use App\Mail\Contact;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
class FrontController extends Controller
{
    public function index()
    {
        $base=Basedata::first(); 
        $data['base']=$base;
        $data['seo_title']=$base->{'seo_title:'.app()->getLocale()};
        $data['seo_desc']=$base->{'seo_desc:'.app()->getLocale()};
        $data['seo_keywords']=$base->{'key_words:'.app()->getLocale()};
        $data['abouts']=Blog::where('type','about_esnad')->get();
        $data['services']=Blog::where('type','service')->get();
        $data['projects']=Blog::where('type','projects')->orderBy('id', 'desc')->take(5)->get();
        $data['jobs']=Blog::where('type','jobs')->get();
        $data['range_index']=true;
        $data['sliders']=Slider::all();
        $data['clients']=Client::all();
        return view('front.index')->with($data);
    }
    public function page($title)
    {
        $base=Basedata::first(); 
        $data['base']=$base;
        $title=str_replace('_',' ',$title);
        if($blogtrans=BlogTranslation::where('title',$title)->first()){
            $blog=$blogtrans->blog;
            $data['blog']=$blog;
            $data['seo_title']=$blog->{'seo_title:'.app()->getLocale()};
            $data['seo_desc']=$blog->{'seo_desc:'.app()->getLocale()};
            $data['seo_keywords']=$blog->{'key_words:'.app()->getLocale()};
            $data['abouts']=Blog::where('type','about_esnad')->get();
            $data['services']=Blog::where('type','service')->get();
            $data['jobs']=Blog::where('type','jobs')->get();
            $data['title']=$blog->{'title:'.app()->getLocale()};
            if(!$blog->parent_id){
                $data['sub_blogs']=Blog::where('parent_id',$blog->id)->get();
            }
            return view('front.blog')->with($data);
        }else{
            return abort(404);
        }
    }
    public function videos()
    {
        $base=Basedata::first(); 
        $data['base']=$base;
        $data['videos']=Tv::all();
        $seo=Seo::where('type','tv')->first();
        $data['seo_title']=$seo->{'seo_title:'.app()->getLocale()};
        $data['seo_desc']=$seo->{'seo_desc:'.app()->getLocale()};
        $data['seo_keywords']=$seo->{'key_words:'.app()->getLocale()};
        $data['abouts']=Blog::where('type','about_esnad')->get();
        $data['services']=Blog::where('type','service')->get();
        $data['jobs']=Blog::where('type','jobs')->get();
        $data['title']=trans('site.tv_videos');
        return view('front.pages.videos')->with($data);
    }
    public function news()
    {
        $base=Basedata::first(); 
        $data['base']=$base;
        $seo=Seo::where('type','news')->first();
        $data['seo_title']=$seo->{'seo_title:'.app()->getLocale()};
        $data['seo_desc']=$seo->{'seo_desc:'.app()->getLocale()};
        $data['seo_keywords']=$seo->{'key_words:'.app()->getLocale()};
        $data['abouts']=Blog::where('type','about_esnad')->get();
        $data['services']=Blog::where('type','service')->get();
        $data['jobs']=Blog::where('type','jobs')->get();
        $data['news']=Blog::where('type','news')->get();
        $data['title']=trans('site.news');
        return view('front.pages.news')->with($data);
    }
    public function one_news($title)
    {
        $base=Basedata::first(); 
        $data['base']=$base;
        $blogtrans=BlogTranslation::where('title',$title)->first();
        $blog=$blogtrans->blog;
        $data['blog']=$blog;
        $data['seo_title']=$blog->{'seo_title:'.app()->getLocale()};
        $data['seo_desc']=$blog->{'seo_desc:'.app()->getLocale()};
        $data['seo_keywords']=$blog->{'key_words:'.app()->getLocale()};
        $data['abouts']=Blog::where('type','about_esnad')->get();
        $data['services']=Blog::where('type','service')->get();
        $data['jobs']=Blog::where('type','jobs')->get();
        $data['news']=Blog::where('type','news')->get();
        $data['title']=$blog->{'title:'.app()->getLocale()};
        return view('front.pages.one_news')->with($data);
    }
    public function clients()
    {
        $base=Basedata::first(); 
        $data['base']=$base;
        $data['clients']=Client::all();
        $seo=Seo::where('type','client')->first();
        $data['seo_title']=$seo->{'seo_title:'.app()->getLocale()};
        $data['seo_desc']=$seo->{'seo_desc:'.app()->getLocale()};
        $data['seo_keywords']=$seo->{'key_words:'.app()->getLocale()};
        $data['abouts']=Blog::where('type','about_esnad')->get();
        $data['services']=Blog::where('type','service')->get();
        $data['jobs']=Blog::where('type','jobs')->get();
        $data['news']=Blog::where('type','news')->get();
        $data['title']=trans('site.clients');
        return view('front.pages.clients')->with($data);
    }
    public function slider($title)
    {
        $base=Basedata::first(); 
        $data['base']=$base;
        $data['clients']=Client::all();
        $slidertrans=SliderTranslation::where('title',$title)->first();
        $slider=$slidertrans->slider;
        $data['slider']=$slider;
        $data['seo_title']=$slider->{'seo_title:'.app()->getLocale()};
        $data['seo_desc']=$slider->{'seo_desc:'.app()->getLocale()};
        $data['seo_keywords']=$slider->{'key_words:'.app()->getLocale()};
        $data['abouts']=Blog::where('type','about_esnad')->get();
        $data['services']=Blog::where('type','service')->get();
        $data['jobs']=Blog::where('type','jobs')->get();
        $data['news']=Blog::where('type','news')->get();
        $data['title']=$slider->{'title:'.app()->getLocale()};
        return view('front.pages.slider')->with($data);
    }
    public function contact_us()
    {
        $base=Basedata::first(); 
        $data['base']=$base;
        $seo=Seo::where('type','contactus')->first();
        $data['seo_title']=$seo->{'seo_title:'.app()->getLocale()};
        $data['seo_desc']=$seo->{'seo_desc:'.app()->getLocale()};
        $data['seo_keywords']=$seo->{'key_words:'.app()->getLocale()};
        $data['abouts']=Blog::where('type','about_esnad')->get();
        $data['services']=Blog::where('type','service')->get();
        $data['jobs']=Blog::where('type','jobs')->get();
        $data['news']=Blog::where('type','news')->get();
        $data['title']=trans('site.contact_us');
        return view('front.pages.contact_us')->with($data);
    }
    public function job()
    {
        $base=Basedata::first(); 
        $data['base']=$base;
        $data['clients']=Client::all();
        $seo=Seo::where('type','jobs')->first();
        $data['seo_title']=$seo->{'seo_title:'.app()->getLocale()};
        $data['seo_desc']=$seo->{'seo_desc:'.app()->getLocale()};
        $data['seo_keywords']=$seo->{'key_words:'.app()->getLocale()};
        $data['abouts']=Blog::where('type','about_esnad')->get();
        $data['services']=Blog::where('type','service')->get();
        $data['jobs']=Blog::where('type','jobs')->get();
        $data['news']=Blog::where('type','news')->get();
        $data['title']=trans('site.jobs');
        return view('front.pages.job')->with($data);
    }
    public function add_job(Request $request)
    {
        $request->validate([
            'name'=>'required|min:3',
            'job_title'=>'required',
            'cv'=>v_file(),
        ]);
        $data=$request->except(['_token','cv']);
        if ($request->hasFile('cv')) {
            $data['cv']=cv_upload($request->cv);
        }
        Job::create($data);
        session()->flash('front_success',trans('site.cv_uploaded_sucessfully'));
        return back();
    }
    public function send_message(Request $request)
    {
        $request->validate([
            'email'=>'required|email',
            'phone'=>'required|min:4',
        ]);
        \Mail::to('gebril.m@yahoo.com')->send(new Contact($request->email,$request->phone,$request->message));
        //dd($request);
//        $mail = new PHPMailer(true);                            // Passing `true` enables exceptions
//
//            // Server settings
//        $mail->SMTPDebug = 0;                                   // Enable verbose debug output
//            $mail->isSMTP();                                        // Set mailer to use SMTP
//            $mail->Host = 'smtp.gmail.com';                                             // Specify main and backup SMTP servers
//            $mail->SMTPAuth = true;                                 // Enable SMTP authentication
//            $mail->Username = 'gebrilmahmoud519@gmail.com';             // SMTP username
//            $mail->Password = 'semsem12';              // SMTP password
//            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
//            $mail->Port = 587;                                    // TCP port to connect to
//
//            //Recipients
//            $mail->CharSet  = 'UTF-8';
//            $mail->setFrom('gebrilmahmoud519@gmail.com','Website' );
//            $mail->addAddress('gebril.m@yahoo.com', 'Website'); // Add a recipient, Name is optional
//            $mail->addReplyTo('gebrilmahmoud519@gmail.com','Website');
//            $mail->addCC('gebril.m@yahoo.com');
//            //$mail->addBCC('gebril.m@yahoo.com');
//
//            //Attachments (optional)
//            // $mail->addAttachment('/var/tmp/file.tar.gz');            // Add attachments
//            // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');   // Optional name
//
//            //Content
//            $mail->isHTML(true);                                                                    // Set email format to HTML
//            $mail->Subject = ' New Message';
//            $mail->Body    = '<center>
//                                <table border="0" cellspacing="0" cellpadding="0" align="center" width="520" bgcolor="#ffffff" style="background:#ffffff;min-width:520px">
//                                  <tbody><tr>
//                                    <td width="20" bgcolor="#eeeeee" style="background:#eeeeee"></td>
//                                      <td width="480">
//                                      <table border="0" cellspacing="0" cellpadding="0" width="100%">
//                                                <tbody><tr>
//                                                    <td height="20" bgcolor="#eeeeee" style="background:#eeeeee"></td>
//                                                </tr>
//                                                <tr>
//                                                    <td>
//                                                 <table border="0" cellspacing="0" cellpadding="0" align="center" width="100%" style="border-bottom:1px solid #eeeeee">
//                                            <tbody><tr>
//                                                    <td height="49"></td>
//                                             </tr>
//
//
//                                              <tr>
//                                                  <td height="20"></td>
//                                              </tr>
//                                              <tr>
//                                             <td align="center" class="m_3390709613411428861whom" style="color:#4285f4;font-family:&quot;Roboto&quot;,OpenSans,&quot;Open Sans&quot;,Arial,sans-serif;font-size:32px;font-weight:normal;line-height:46px;margin:0;padding:0 25px 0 25px;text-align:center">Phone : '.$request->phone.'</td>
//                                             </tr>
//
//                                              <tr>
//                                                  <td height="20"></td>
//                                              </tr>
//                                            </tbody></table>
//                                                    </td>
//                                                </tr>
//                                        <tr>
//                                          <td>
//                                            <table border="0" cellspacing="0" cellpadding="0" align="center" width="100%" style="border-bottom:1px solid #eeeeee">
//                                            <tbody><tr>
//                                                <td height="32"></td>
//                                             </tr>
//                                             <tr>
//                                                <td width="60" height="77" align="center" style="font-size:8px">
//                                              <a  style="color:#4285f4;text-decoration:underline" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.google.com/appserve/mkt/p/AFnwnKUQgm7SAbntJi3oNxXHEoUsEBRVkYtVgIU3JUqWBonGUN2P1JMIpaMctDQHBHpB3ug7waawMKwovHEl2PhJwcZjsAuarHUlupt78VgTApLkpOAATdfzDD-3yleTAbDNPmwAKO5gVTP9bwHXMBB10f2DSkjR3qHRWw&amp;source=gmail&amp;ust=1565961215100000&amp;usg=AFQjCNEVicl6tQ_rtDbpVM_xMOnmnFT__w">
//                                             <img width="60" height="77" src="https://ci5.googleusercontent.com/proxy/qfu7SEHh1TSi6TCN16e95iHethko_7co68MVV4pC3hGMkOQNNuiTYb5p5QscK3HBhUuoZNTd8k485egZyjvTVvXP5gnTIJLS7yrLc4wnVKGPJA6YVA=s0-d-e1-ft#https://services.google.com/fh/files/emails/shield_logo_150_rt.png" alt="Security" border="0" style="width:60px;height:77px;text-align:center;border:none" class="CToWUd">
//                                              </a>
//                                              </td>
//                                             </tr>
//                                             <tr>
//                                                <td height="15"></td>
//                                             </tr>
//                                             <tr>
//                                                <td align="center" class="m_3390709613411428861device_txt" style="color:#757575;font-family:&quot;Roboto&quot;,OpenSans,&quot;Open Sans&quot;,Arial,sans-serif;font-size:24px;font-weight:normal;line-height:33px;margin:0;padding:0 25px 0 25px;text-align:center">Message From '.$request->email.'</td>
//                                             </tr>
//
//                                             <tr>
//                                                <td height="4" style="line-height:4px;font-size:4px"></td>
//                                             </tr>
//
//                                             <tr>
//                                               <td align="center" class="m_3390709613411428861para" style="color:#757575;font-family:&quot;Roboto&quot;,OpenSans,&quot;Open Sans&quot;,Arial,sans-serif;font-size:17px;font-weight:normal;line-height:24px;margin:0;padding:0 25px 0 25px;text-align:center"> '.$request->message.'
//                                               </td>
//                                             </tr>
//                                             <tr>
//                                                <td height="30"></td>
//                                             </tr>
//                                           </tbody></table>
//                                          </td>
//                                        </tr>
//
//
//
//                                                <tr>
//                                                    <td height="18" bgcolor="#eeeeee" style="background:#eeeeee"></td>
//                                                </tr>
//                                            </tbody></table>
//                                        </td>
//                                        <td width="20" bgcolor="#eeeeee" style="background:#eeeeee"></td>
//                                    </tr>
//                                </tbody></table>
//                                <div style="display:none;white-space:nowrap;font:15px courier;line-height:0">
//                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
//                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
//                                </div>
//                                </center>';                     // message
//
//            $mail->send();
            
            return back()->with('sending_success', 'Sent!');
    }
}
