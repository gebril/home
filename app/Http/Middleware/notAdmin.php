<?php

namespace App\Http\Middleware;

use Closure;

class notAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (!Auth()->check()) {

            return $next($request);
        }

        return redirect(aurl('admin'))->with('error' , 'You Are Already Logged In');

    }
}
