<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasedatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('basedatas', function (Blueprint $table) {
            $table->increments('id');
            $table->text('email');
            $table->text('number1');
            $table->text('number2');
            $table->text('logo');
            $table->text('url');
            $table->text('facebook');
            $table->text('twitter');
            $table->text('instgram');
            $table->text('youtube');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('basedatas');
    }
}
