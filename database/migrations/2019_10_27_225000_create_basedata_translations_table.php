<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasedataTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('basedata_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->text('address');
            $table->text('vision');
            $table->text('message');
            $table->text('founder');
            $table->text('seo_title')->nullable();
            $table->text('seo_desc')->nullable();
            $table->text('key_words')->nullable();
            $table->integer('basedata_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['basedata_id','locale']);
            $table->foreign('basedata_id')->references('id')->on('basedatas')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('basedata_translations');
    }
}
