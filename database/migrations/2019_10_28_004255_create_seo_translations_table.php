<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeoTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seo_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->text('seo_title')->nullable();
            $table->text('seo_desc')->nullable();
            $table->text('key_words')->nullable();
            $table->integer('seo_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['seo_id','locale']);
            $table->foreign('seo_id')->references('id')->on('seos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seo_translations');
    }
}
