<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTvTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tv_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title');
            $table->integer('tv_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['tv_id','locale']);
            $table->foreign('tv_id')->references('id')->on('tvs')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tv_translations');
    }
}
