<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogcatTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogcat_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->text('content');
            $table->integer('blogcat_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['blogcat_id','locale']);
            $table->foreign('blogcat_id')->references('id')->on('blogcats')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogcat_translations');
    }
}
