<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogseoTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogseo_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->text('seo_title');
            $table->text('seo_desc');
            $table->text('key_words');
            $table->integer('blogseo_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['blogseo_id','locale']);
            $table->foreign('blogseo_id')->references('id')->on('blogseos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogseo_translations');
    }
}
